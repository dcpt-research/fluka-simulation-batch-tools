# loading measurements
import sys
import numpy as np
import matplotlib.pyplot as plt

measurement_filename = sys.argv[1]
fluka_filename = sys.argv[2]

measurement_x = []
measurement_y = []
measurement_dose = []

fluka_x_or_y = []
fluka_dose = []
fluka_dose_uncert_in_percent = []

with open(measurement_filename,'r') as f:
	for line in f:
		if line.split()[0] == "=":
			measurement_x.append(float(line.split()[1]))
			measurement_y.append(float(line.split()[2]))
			measurement_dose.append(float(line.split()[4]))

norm_min = -5
norm_max = 5

fluka_bins = 720
fluka_bin_min_x_or_y = -180
fluka_bin_max_x_or_y = 180
fluka_bin_size = (fluka_bin_max_x_or_y - fluka_bin_min_x_or_y) / fluka_bins

skip_lines = 9
skip_uncert = 2
uncert_from = -1
swapped_to_uncert = False
with open(fluka_filename,'r') as f:
	for i,line in enumerate(f):
		if swapped_to_uncert:
			# do something
			if i < uncert_from + skip_uncert:
				continue
			for dose_uncert in line.split():
				fluka_dose_uncert_in_percent.append(float(dose_uncert))
			continue
		if i < skip_lines:
			continue
		if line.split() == []:
			swapped_to_uncert = True
			uncert_from = i
		else:
			for dose in line.split():
				fluka_dose.append(float(dose))

fluka_x_or_y = [fluka_bin_min_x_or_y + fluka_bin_size * (i + 0.5) for i in range(fluka_bins)]



fluka_x_or_y = np.array(fluka_x_or_y)
fluka_dose = np.array(fluka_dose)
fluka_dose_uncert_in_percent = np.array(fluka_dose_uncert_in_percent)

measurement_x = np.array(measurement_x)
measurement_y = np.array(measurement_y)
measurement_dose = np.array(measurement_dose)

use_y = True
use_x = True

if all(measurement_x == 0):
	use_x = False
if all(measurement_y == 0):
	use_y = False



count=0
fluka_dose_normalization = 0
for i in range(len(fluka_x_or_y)):
	if fluka_x_or_y[i] > norm_min and fluka_x_or_y[i] < norm_max:
		count += 1
		fluka_dose_normalization = fluka_dose_normalization + fluka_dose[i]
fluka_dose_normalization = fluka_dose_normalization / count

count=0
measurement_dose_normalization = 0
if use_x:
	for i in range(len(measurement_x)):
		if measurement_x[i] > norm_min and measurement_x[i] < norm_max:
			count += 1
			measurement_dose_normalization = measurement_dose_normalization + measurement_dose[i]
if use_y:
	for i in range(len(measurement_y)):
		if measurement_y[i] > norm_min and measurement_y[i] < norm_max:
			count += 1
			measurement_dose_normalization = measurement_dose_normalization + measurement_dose[i]
measurement_dose_normalization = measurement_dose_normalization / count

measurement_dose = measurement_dose / measurement_dose_normalization * 100
fluka_dose = fluka_dose / fluka_dose_normalization * 100

fluka_dose_uncert = fluka_dose_uncert_in_percent / 100

fluka_dose_uncert_absolute = fluka_dose_uncert * fluka_dose

plt.figure()
plt.errorbar(fluka_x_or_y, fluka_dose, yerr = fluka_dose_uncert_absolute, color="blue", label="FLUKA MC")
plt.ylabel("Dose [%]")
if use_x:
	plt.plot(measurement_x, measurement_dose, color="red", label="Measurement")
	plt.xlabel("x [mm]")
if use_y:
	plt.plot(measurement_y, measurement_dose, color="red", label="Measurement")
	plt.xlabel("y [mm]")
plt.legend()

plt.show()