#!/usr/bin/env python3
import pexpect
import argparse
import os
import code
import readline
import rlcompleter
import time


def main():
    args = parser.parse_args()
    fres_path = ""
    if args.FRES:
        fres_path = args.FRES[0]
    else:
        if os.getenv("FRES"):
            fres_path = os.getenv("FRES")
        else: 
            # we neither have the shell variable or the supplied path.
            # Get mad, throw a fit - make the user know they fucked up
            # get MAAAD at the user!!
            print("You are supposed to either have a environment variable named FRES")
            print("with the path to the FRES repository or supply the repository path")
            print("using --FRES /path/to/FRES")
            print("Thank you for understanding.")
            exit()
    patient_dir = args.patient[0]
    scoring_dir = args.bnnlis[0]

    silentmode = args.silent
    ctvfallback  = args.ctvfallback 


    exp_patient_path = "Provide the directory of the DICOM files"
    exp_scoring_path = "Provide the directory of the FLUKA scoring files"
    exp_quantities = "Physical dose"; ans_quantities = "1 2 4"
    exp_weighing = "want to weigh the treatment"; ans_weighing = "2"
    exp_plans = "plans available. Choose the one"; ans_plans = ""
    exp_bnnlis = ".bnn.lis"; ans_bnnlis = ""
    exp_weighby = "Weight beams by dose or FinalCumulativeMeterset"; ans_weighby = ""
    exp_structs = "Provide the normalization volume" ; ans_structs = ""
    exp_normmethod = "Select a normalization method"; ans_normmethod = "4"
    exp_difference = "Found the following differences"; ans_difference = ""
    # now just expect and send all possible things and done!! Wupti :)


    if not silentmode: print("Spawning process")
    child = pexpect.spawn("python3 {}/convert_to_dicom.py ds".format(fres_path))
    child.expect(exp_patient_path) 
    if not silentmode: print("Prompted for patient directory")
    child.sendline(patient_dir)
    if not silentmode: print("Provided patient directory: {}".format(patient_dir))

    child.expect(exp_scoring_path) 
    if not silentmode: print("Prompted for scoring directory")
    child.sendline(scoring_dir)
    if not silentmode: print("Provided scoring directory: {}".format(scoring_dir))


    child.expect(exp_quantities)
    if not silentmode: print("Prompted for quantities")
    child.sendline(ans_quantities)
    if not silentmode: print("Provided quantities: {}".format(ans_quantities))

    child.expect(exp_weighing)
    if not silentmode: print("Prompted for weighing")
    child.sendline(ans_weighing)
    if not silentmode: print("Provided weighing: {}".format(ans_weighing))

    child.expect(exp_plans)
    if not silentmode: print("Prompted for plans")
    child.sendline(ans_plans)
    if not silentmode: print("Provided plans: {}".format(ans_plans))

    for i in range(3):
        child.expect(exp_bnnlis)
        if not silentmode: print("Prompted for bnnlis")
        child.sendline(ans_bnnlis)
        if not silentmode: print("Provided bnnlis: {}".format(ans_bnnlis))

    child.expect(exp_weighby)
    if not silentmode: print("Prompted for weighby")
    child.sendline(ans_weighby)
    if not silentmode: print("Provided weighby: {}".format(ans_weighby))

        
    struct_or_diff = child.expect([exp_structs, exp_difference], timeout = 180) # waits up to 3 minutes. Should not be necessary
    if struct_or_diff == 1:
        if not silentmode: print("Prompted for differences")
        child.sendline(ans_difference)
        if not silentmode: print("Said OK to the differences")

        # now we wait for the structure message only if we saw the difference message
        # because if we did not see the difference message then we have already waited
        # for the structure message
        child.expect(exp_structs, timeout = 180)
        if not silentmode: print("Prompted for structs")

        
    if not silentmode: print("Reading the structures")
    list_of_outputs = str(child.before).replace("\\n","").split("Type")[-1].split("\\r")
    ctv_number = -1
    for line in list_of_outputs:
        if not silentmode: print(line)
        if "ctv" in line.lower():
            # This line has the CTV. The number will be the first symbol
            # or the first 2
            ctv_number = int(line[:2])
    if ctv_number == -1:
        # we did not find the CTV
        if not silentmode: print("No CTV found, falling back to {}".format(ctvfallback))
        if ctvfallback:
            ctv_number = -1
            for line in list_of_outputs:
                if not silentmode: print(line)
                if ctvfallback in line.lower():
                    # This line has the CTV. The number will be the first symbol
                    # or the first 2
                    ctv_number = int(line[:2])
            
        if ctv_number == -1:
            print("We were unable to find the CTV in the list of structures.")
            print("Handle this one manually.")
            print("A good idea is to run:")
            print("for line in list_of_outputs:")
            print(" print(line)")
            print("And then set ctv_number to the number corresponding to the structure you wish to normalize to")
            print("And then CTRL-D to exit the interactive console")
            vars = globals()       
            vars.update(locals())
            readline.set_completer(rlcompleter.Completer(vars).complete) 
            readline.parse_and_bind("tab: complete")                     
            code.InteractiveConsole(vars).interact()    
            #exit()
    ans_structs = str(ctv_number)
            
    child.sendline(ans_structs)
    if not silentmode: print("Provided struct: {}".format(ans_structs))

        
    child.expect(exp_normmethod)
    if not silentmode: print("Prompted for normmethod")
    #if not silentmode: print(child.before)
    child.sendline(ans_normmethod)
    #child.sendline(ans_normmethod)
    if not silentmode: print("Provided normmethod: {}".format(ans_normmethod))

    maxtime = 60  # wait for a maximum of 60 seconds for processes to conclude
    t0 = time.time()
    t1 = time.time()
    while child.isalive() and t1-t0 <= maxtime:
        t1 = time.time()

    # vars = globals()     
    # vars.update(locals())
    # readline.set_completer(rlcompleter.Completer(vars).complete) 
    # readline.parse_and_bind("tab: complete")                   
    #code.InteractiveConsole(vars).interact()   


            


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Tool for automagically converting FLUKA dose and LET simulations to DICOM RTDOSE. ')
    parser.add_argument('--patient', type=str, nargs=1, required=True,\
                help='Path to patient directory containing FRES FLUKA plans')   
    parser.add_argument('--bnnlis', type=str, nargs=1, required=True,\
                help='Path to FLUKA scoring files (.bnn.lis) files')    
    parser.add_argument('--FRES', type=str, nargs=1, required=False,\
                help='Path to FRES repository. Either specify here or "export FRES=/path/to/FRES".')
    parser.add_argument('--silent', action='store_true', required=False,\
                help='Supply this flag if you want no output to the terminal')
    parser.add_argument('--ctvfallback', type=str, required=False,\
                help='What structure should we fall back to in case no CTV is found for normalization')
        
    args = parser.parse_args()
    main()
