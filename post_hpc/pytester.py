#!/usr/bin/env python3
def main():
    print("Starting main")
    a = input("What value should a have? Input below: \n>>> ")
    print("a was set to {}".format(a))
    b = input("Do you wish to change a? (enter for yes, n for no): \n>>> ")
    if b == "":
        print("You wish to change a!")
    else:
        print("You are happy with the current value of a.")

if __name__ == "__main__":
    main()
