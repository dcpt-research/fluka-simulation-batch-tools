#!/usr/bin/env bash
## calculates 1,2,3 %/mm gamma passes over 10% mindose for all patient directories in this folder.
batch=$1

if [[ $batch == "" || $batch == "-h" ]];
then
	echo "Please provide path to the batch for which you want to calculate all"
	echo "gamma passes"
	echo "Usage: ./calc_all_gamma_passes_in_batch.sh /path/to/batch/"
	exit 0
fi

for fold in $batch/*;
do
	/home/rasmus/Documents/cli-dose-comparison/gamma_pass.sh "${fold}" --dose-dta 1 --dist-dta 1 -o "gamma_pass_11.txt"
	/home/rasmus/Documents/cli-dose-comparison/gamma_pass.sh "${fold}" --dose-dta 2 --dist-dta 2 -o "gamma_pass_22.txt"
	/home/rasmus/Documents/cli-dose-comparison/gamma_pass.sh "${fold}" --dose-dta 3 --dist-dta 3 -o "gamma_pass_33.txt"
done
