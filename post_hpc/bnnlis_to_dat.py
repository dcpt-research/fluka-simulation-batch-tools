#!/usr/bin/env python3
# Skip fluka lines in beginning
import argparse

def write_bnnlis_to_dat(bnnlis, outputfile, x0, x1, n):
	dx = (x1-x0)/n


	counting = -1

	fluka_data = [[],[]]



	put_in_i = 0
	with open(bnnlis,'r') as flk_file:
		for line in flk_file:
			counting += 1
			if counting < 9:
				continue
			if line.split() == []:
				counting = 6
				put_in_i = 1
			for dose in line.split():
				fluka_data[put_in_i].append(float(dose))

	maxdose = max(fluka_data[0])
	fluka_data[0] = [d/maxdose for d in fluka_data[0]]

	data = []
	for i in range(n):
		left_edge = x0 + i * dx
		right_edge = x0 + (i+1) * dx
		data.append("{:.10E} {:.10E} {:.5E} {:.5E}\n".format(left_edge, right_edge, fluka_data[0][i], fluka_data[1][i]))

	with open(outputfile, 'w') as f:
		f.writelines(data)

if __name__=="__main__":
	parser = argparse.ArgumentParser(description='Tool for transforming a .bnn.lis file to a .dat file as created by FLUKA flair plotting.')
	parser.add_argument('-i', type=str, nargs=1, required=True, help=".bnn.lis file path to be made into .dat file")
	parser.add_argument('-o', type=str, nargs=1, required=True, help="Name of output file (should ideally be .dat)")
	parser.add_argument('--x0', type=str, nargs=1, required=True, help="Coordinate start. Assuming 1 dimensional data, can not be used for >1 dimensional.")
	parser.add_argument('--x1', type=str, nargs=1, required=True, help="Coordinate end. Assuming 1 dimensional data, can not be used for >1 dimensional.")
	parser.add_argument('-n', type=str, nargs=1, required=True, help="Number of bins.")

	args = parser.parse_args()

	bnnlis = args.i[0]
	datfile = args.o[0]
	x0 = float(args.x0[0])
	x1 = float(args.x1[0])
	n = int(args.n[0])

	write_bnnlis_to_dat(bnnlis, datfile, x0, x1, n)
