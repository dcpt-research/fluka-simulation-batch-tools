#!/usr/bin/env bash
function usage()
{
    echo "Tool for finding all the patients in the converted patient "
    echo "folders that have been converted, or tried to be converted "
    echo "Usage:"
    echo "$0 [OPTION] [folders_to_look_in...]"
    echo "OPTIONS:"
    echo "  -h, --help                      Print this help message"
    exit 1
}


if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            folder_list+=("$1")
            shift
            ;;
    esac
done


for fold in ${folder_list[@]};
do
    if [[ $(find ${fold} -mindepth 1 -type d | wc -l) -ge 1 ]];
    then
        for p in $(ls -d ${fold}/*/);
        do
            basename $p
        done
    fi
done | sort | uniq