#!/usr/bin/env python3
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import os
import code
import sys

def read_gammas(filename):
	return_dict = {} # struct:gamma pass
	with open(filename,'r') as f:
		for line in f:
			if "Gamma:" in line:
				# This is a line with a gamma pass 			
				strp_line = line.replace(" ","").replace("%","").replace("\n","")
				strp_line = strp_line.replace("Gamma:","")
				struct, num_str=strp_line.split("=")
				try: 
					num = float(num_str)
				except ValueError:
					num = -1
				return_dict[struct] = num

	return return_dict
def main(arg):
	basepath = arg
	if basepath[-1] == "/":
		basepath = basepath[:-1]
	gammas={} # patient : DTA : structure : gammapass
	for fold in next(os.walk(basepath+"/"))[1]:
		for pat_fold in next(os.walk(basepath+"/"+fold))[1]:
			patient_dict = {}
			for p in ["11","22","33"]:  # 1mm / 1% , 2mm / 2% and 3mm / 3%
				# Here we assume a folder more than neccesary.. .But ok
				patient_dict[p] = read_gammas(basepath+"/"+fold+"/"+pat_fold+"/"+"gamma_pass_"+p+".txt")
			gammas[pat_fold] = patient_dict


	#plt.figure()
	lowers = {} # pat : struct : gammapass
	middles= {} 
	uppers = {} 
	for dta in ["11","22","33"]:
		for pat in gammas.keys():
			this_pat_low = {}
			this_pat_mid = {}
			this_pat_upp = {}
			for struct in gammas[pat][dta].keys():
				if dta == "11":
					this_pat_low[struct] = gammas[pat][dta][struct]
				if dta == "22":
					this_pat_mid[struct] = gammas[pat][dta][struct]
				if dta == "33":
					this_pat_upp[struct] = gammas[pat][dta][struct]
			if dta == "11":
				lowers[pat]=this_pat_low
			if dta == "22":
				middles[pat]=this_pat_mid
			if dta == "33":
				uppers[pat]=this_pat_upp


	allstructs=list(lowers[list(gammas.keys())[0]].keys())
	fmts_all = ["^", "s", "o", "D", "x", "*"]
	fmts={}
	for i,struct in enumerate(allstructs):
		fmts[struct] = fmts_all[i]
		
	#fmts = {0:"^", 1: "s", 2: "o", 3: "D", 4: "x"}
	xval = 1 # patient number


	# lowest body passrate
	lowest_body_11 = np.min([gammas[list(gammas.keys())[i]]["11"]["BODY"] for i in range(len(gammas.keys()))])
	lowest_body_22 = np.min([gammas[list(gammas.keys())[i]]["22"]["BODY"] for i in range(len(gammas.keys()))])
	lowest_body_33 = np.min([gammas[list(gammas.keys())[i]]["33"]["BODY"] for i in range(len(gammas.keys()))])




	font = {'family' : 'normal',
		'weight' : 'bold',
		'size'   : 16}

	matplotlib.rc('font', **font)

	plt.figure()
	
	plt.plot([-100000000,-100000000],[-100000000,-1000000000],color="green",label="3 mm / 3 %")
	plt.plot([-100000000,-100000000],[-100000000,-1000000000],color="blue",label="2 mm / 2 %")
	plt.plot([-100000000,-100000000],[-100000000,-1000000000],color="red",label="1 mm / 1 %")
	plt.plot([-100000000,-100000000],[-100000000,-1000000000],'--',color="black",label="Body minimum")

	minbody_11 = 100
	minbody_22 = 100
	minbody_33 = 100

	for i,pat in enumerate(gammas.keys()):
		xdif = -0.2
		for struct in lowers[pat].keys():
			nom = middles[pat][struct]
			lower_err = - lowers[pat][struct] + nom
			upper_err =  uppers[pat][struct] - nom
			if nom == -1:
				xdif = xdif + 0.1
				continue
			if struct == "BODY":
				if nom+upper_err < minbody_33:
					minbody_33 = nom+upper_err
				if nom           < minbody_22:
					minbody_22 = nom
				if nom-lower_err < minbody_11:
					minbody_11 = nom-lower_err
			#plt.errorbar([xval+xdif],[nom]
				#,yerr=[[lower_err],[upper_err]]
				#,fmt=fmts[struct],color="blue",markersize=6, capsize=3)
			plt.plot([xval+xdif],[nom+upper_err],fmts[struct],color="green",markersize=6)
			plt.plot([xval+xdif],[nom],fmts[struct],color="blue",markersize=6,label=struct if i==0 else "")
			plt.plot([xval+xdif],[nom-lower_err],fmts[struct],color="red",markersize=6)
			## and a black line through them all
			plt.plot([xval+xdif]*2,[nom-lower_err,nom+upper_err],color="black")
			xdif = xdif + 0.1
		xval = xval + 1
	
	#plt.plot([0,xval+1],[lowest_body_33,lowest_body_33],'--',color="grey")
	#plt.plot([-1000,xval+1000],[90,90],'--',color="green")
	
	# plot the lowest green
	plt.plot([-10000,10000],[minbody_11]*2, '--',color = "red")#, label="BODY min 1 mm / 1 % = {:.2f}".format(minbody_11))
	
	# plot the lowest blue
	plt.plot([-10000,10000],[minbody_22]*2, '--',color = "blue")#, label="BODY min 2 mm / 2 % = {:.2f}".format(minbody_22))

	# plot the lowest red
	plt.plot([-10000,10000],[minbody_33]*2, '--',color = "green")#, label="BODY min 3 mm / 3 % = {:.2f}".format(minbody_33))



	plt.xlabel("Patient number")
	plt.ylabel("Gamma pass rating [%]")
	plt.xticks(np.arange(1,xval+1,1))
	plt.yticks(np.arange(0,101,10))
	#plt.legend(fmts.keys())
	plt.legend()
	plt.xlim([0,xval])
	plt.ylim([0,105])
	plt.grid()
	#plt.savefig("gamma_pass_all_simulations.svg")
	plt.show()


	for i,up_mid_low in enumerate([lowers,middles,uppers]):
		all_passrates = {s:[] for s in list(up_mid_low.values())[0].keys()} # struct : list of passrates 3mm/3%
		# up_mid_low = pat : struct : gamma for 3mm/3%
		for pat in up_mid_low.keys():
			for struct in up_mid_low[pat].keys():
				if up_mid_low[pat][struct] != -1:
					all_passrates[struct].append(up_mid_low[pat][struct])
		fig,ax = plt.subplots()
		ax.boxplot(list(all_passrates.values()))
		ax.set_xticklabels(list(all_passrates.keys()))
		ax.set_ylabel("Gamma pass rates [%]")
		ax.set_title("FLUKA dose vs PLAN dose {0}mm / {0}%".format(i+1))
		ax.grid()
		#plt.savefig("gamma_pass_3mm3p_boxplot.svg")
		plt.show()
	
		
				
		#for struct in gammas[pat][list(gammas[pat].keys())[0]]:
			#print(struct)

if __name__ == "__main__":
	if "-h" in sys.argv or "--help" in sys.argv:
		print("Prints a distribution of all the gamma pass values for all patients in the supplied folder.")
		print("Gamma comparison files must be written using the cli_dose_comparison module")
		print("Currently patient folders must be in structure like:")
		print("/base_path/group_of_patient_data_folders/patient_folder")
		print("Where you then supply base_path. You can have as many groups with patient folder as you want.")
		print("Usage:")
		print("    python3 progress_gammaplot.py /base_path")
		exit()
	if len(sys.argv) > 1:
		main(sys.argv[1])
	else:
		print("need to supply base path")
		exit(0)
			
			
		

