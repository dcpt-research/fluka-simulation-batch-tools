#!/usr/bin/env python3
import pydicom as pd
import argparse
import readline                                              
import rlcompleter
import code
from scipy.spatial import Delaunay
import numpy as np
import matplotlib.pyplot as plt
import time


def code_interact(globals, locals):
	vars = globals
	vars.update(locals)
	readline.set_completer(rlcompleter.Completer(vars).complete)
	readline.parse_and_bind("tab: complete")
	code.InteractiveConsole(vars).interact()	


def selectStructures(structure, selections):
	"""
		This function takes an RTSTRUCT and a list of wanted structure names
		It then tries to find them automagically based on their names,
		but if unable it will prompt the user for help.
		If one is not available the user can enter this
		and the function will return with the ones found.
		The function returns a list of strings corresponding to the structurenames.
	"""
	iii = None
	for i, el in enumerate(selections):
		if el == "body":
			iii = i
			break
	if not iii == None:
		del selections[iii]

	return_list = ["NOT_FOUND_YET"] * len(selections)
	# first we try to find the correct structures. Then only ask for what we need.
	listOfStructures = [a.ROIName for a in structure.StructureSetROISequence]
	for i, name in enumerate(listOfStructures):
		if name.lower() in [l.lower() for l in selections]:
			sel_indx = -1
			for j,sel in enumerate(selections):
				if sel.lower() == name.lower():
					sel_indx = j
					break
			if sel_indx != -1:
				return_list[sel_indx] = name
	if "NOT_FOUND_YET" not in return_list:
		return return_list
	
	missing_indices = []
	for i, struct in enumerate(return_list):
		if struct == "NOT_FOUND_YET":
			missing_indices.append(i)
	
	missing_structs = [selections[i] for i in missing_indices]
	printStructures(structure)
	print("Please select structures. Select the structures in the given order:")
	print("If you are unable to find a structure, write \"-1\" in that structures place")
	for organ in missing_structs:
		print("\""+organ+"\"" +" ",end="")
	print("")
	selection=input(">>> ").split(" ")
	selection_ints = []
	for indx in selection:
		if indx == "-1":
			continue
		if indx.isnumeric():
			selection_ints.append(int(indx))
	
	structure_names_chosen = [[a.ROIName for a in structure.StructureSetROISequence][indx] for indx in selection_ints]
	for missing_index, missing_struct in zip(missing_indices, structure_names_chosen):
		return_list[missing_index] = missing_struct
	# now we remove all "NOT_FOUND_YET" entries
	final_return_list = []
	for entry in return_list:
		if entry != "NOT_FOUND_YET":
			final_return_list.append(entry)
	return final_return_list
	
def printStructures(structure):
	print("There are the following structures available in your RTSTRUCT:")
	for i, name in enumerate([a.ROIName for a in structure.StructureSetROISequence]):
		print("{0:2}: {1}".format(i,name))
def promptStructure(structure):
	# print structures and ask for a number then return the structname
	printStructures(structure)
	successful = False
	good_numbers = []
	while not successful:
		struct_numbers = input("Please input numbers matching the structures you want a DVH for\n\
		(seperate with spaces).\n>>> ")
		numbers = struct_numbers.split(" ")
		successful = True
		for struct_num in numbers:
			if struct_num.isnumeric():
				good_numbers.append(int(struct_num))
			else:
				print("The input: \"{}\" is not allowed. Try again.".format(struct_num))
				successful = False
		if len(good_numbers) == 0:
			print("No good numbers found. Try again")
			successful = False
	structs = [[a.ROIName for a in structure.StructureSetROISequence][struct_n] for struct_n in good_numbers]
	return structs

def makedvh(rtdose,rtstruct,structname,coord_grid,verbose=False):
	if verbose: print("_____________________ENTERING makedvh()_________________________")
	# read rtdose and make dvh based on rtstruct inside structname
	# make file "structname.txt" with cumulative dvh
	# and save in same folder as rtdose

	# need to get the ROIContourSequenceXyz

	t0=time.time()
	dosegrid = rtdose.pixel_array * rtdose.DoseGridScaling
	t1=time.time()
	if verbose: print("It takes {} seconds to make the dosegrid!".format(round(t1-t0,2)))
	
	t0=time.time()
	struct_contourXyz = getROIContourSequenceXyz(rtstruct,ROIName2Number(rtstruct,structname))
	t1=time.time()
	if verbose: print("It takes {} seconds to get the contour xyz coordinates!".format(round(t1-t0,2)))


	t0=time.time()
	struct_delaunay = Delaunay(struct_contourXyz)
	t1=time.time()
	if verbose: print("It takes {} seconds to make the deluanay object!".format(round(t1-t0,2)))

	# indices in dosegrid where we are inside structname	

	t0=time.time()
	structname_inds = np.where( (struct_delaunay.find_simplex(coord_grid) >= 0) == True) 
	t1=time.time()
	if verbose: print("It takes {0} seconds to find the indices in which the structure {1} is!".format(round(t1-t0,2),structname))


	t0=time.time()
	dose_in_struct = dosegrid[structname_inds] # these are the indices in the dosegrid where we are inside
	t1=time.time()
	if verbose: print("It takes {} seconds to find all the dose values inside the object!".format(round(t1-t0,2)))

	dose_step = 0.2 # gray	

	t0=time.time()
	dvh = [[],[]] # [[di,di+1,...],[vi,vi+1,...]]
	for dos in np.arange(0,np.max(dose_in_struct)+dose_step,dose_step):
		n_indices = len(dose_in_struct[dose_in_struct>=dos])
		volume_percent = n_indices / len(dose_in_struct)*100
		dvh[0].append(dos)
		dvh[1].append(volume_percent)
	t1=time.time()
	if verbose: print("It takes {} seconds to make a list for the DVH and fill it with dose and volume values!".format(round(t1-t0,2)))
	if verbose: print("_____________________EXITING makedvh()_________________________")

	return dvh



def getROIContourSequence(structure, refROINumber):
	"""
	Returns the ROIContourSequence for a given ROI number of the structure given.
	"""
	for seq in structure.ROIContourSequence:	
		if refROINumber == seq.ReferencedROINumber:
			return seq

def ROIName2Number(struct,ROIName):
	return {a.ROIName : a.ROINumber for a in struct.StructureSetROISequence}[ROIName]
def getROIContourSequenceXyz(structure, refROINumber):
	"""
	Returns the contour xyz data in format [[xi,yi,zi]] for the given structure ROI number
	"""
	ROIContourSequence = getROIContourSequence(structure, refROINumber)
	xyz = []
	for seq in ROIContourSequence.ContourSequence:
		for j in range(int(len(seq.ContourData)/3)):
			x = seq.ContourData[3*j+0]
			y = seq.ContourData[3*j+1]
			z = seq.ContourData[3*j+2]
			xyz.append([x,y,z])
	return xyz

def make_coord_array(rtdose):
	x0,y0,z0 = rtdose.ImagePositionPatient
	dx, dy = rtdose.PixelSpacing
	dz = rtdose.SliceThickness
	if type(dz) != float:
		dz = rtdose.GridFrameOffsetVector[1] - rtdose.GridFrameOffsetVector[0]
	nx = rtdose.Columns
	ny = rtdose.Rows
	nz = rtdose.NumberOfFrames
	p = rtdose.pixel_array
	#t0 = time.time()
	#grid = np.zeros((p.shape[0], p.shape[1], p.shape[2], 3))
	#for iz in range(len(grid)):
	#	z = z0 + iz * dz
	#	for iy in range(len(grid[iz])):
	#		y = y0 + dy/2 + iy * dy
	#		for ix in range(len(grid[iz][iy])):
	#			x = x0 + dx/2 + ix * dx
	#			grid[iz][iy][ix][0] = x
	#			grid[iz][iy][ix][1] = y
	#			grid[iz][iy][ix][2] = z
	#t1 = time.time()
	x = np.linspace(x0+dx/2,x0+nx*dx-dx/2,nx)
	y = np.linspace(y0+dy/2,y0+ny*dy-dy/2,ny)
	z = np.linspace(z0,z0+(nz-1)*dz,nz)
	newgrid = np.array(np.meshgrid(x,y,z,indexing="ij")).T
	#t2 = time.time()	
	
	#print("Old method took {0} seconds!".format(t1-t0))
	#print("New method took {0} seconds!".format(t2-t1))
	#if np.array_equal(newgrid,grid):
	#	print("And the two grids are the same!")
	return newgrid



def main():
	args = parser.parse_args()
	dosepath = args.dose[0]
	structpath = args.struct[0]
	if args.optional_extra:
		optional = args.optional_extra[0]
	else:
		optional = ""
	verbose = args.verbose

		
	
	rtdose = pd.read_file(dosepath)
	rtstruct = pd.read_file(structpath)

	structs = []
	if args.structures:
		structs = selectStructures(rtstruct, args.structures)
	else:
		structs = promptStructure(rtstruct)
	if len(structs) == 0:
		print("No structures found. Exiting")
		exit(0)

	coord_grid = make_coord_array(rtdose)
	dvhs = [makedvh(rtdose,rtstruct,struct,coord_grid,verbose=verbose) for struct in structs]
	for struct,dvh in zip(structs,dvhs):
		dosefolder = ""
		for s in dosepath.split("/")[:-1]:
			dosefolder = dosefolder + s + "/"
		try:
			with open(dosefolder+"dvh-"+struct+optional+".txt","w") as f:
				for i in range(len(dvh[0])):
					f.write("{:5.3f} {:5.3f}\n".format(dvh[0][i],dvh[1][i]))
		except FileNotFoundError:
			print("The file {0} could not be opened.".format(dosefolder+struct+optional+".txt"))
			print("Do you wish to save to another filename?")
			newfilename = input(">>> {0}".format(dosefolder))
			with open(dosefolder+newfilename,"w") as f:
				for i in range(len(dvh[0])):
					f.write("{:5.3f} {:5.3f}\n".format(dvh[0][i],dvh[1][i]))
	
	#vars = globals()       
	#vars.update(locals())
	#readline.set_completer(rlcompleter.Completer(vars).complete) 
	#readline.parse_and_bind("tab: complete")                     
	#code.InteractiveConsole(vars).interact()	


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Tool for creating dose volume histogram files. ')
	# parser.add_argument('-i', type=str, nargs=1, required=True,\
	# 			help='Directory where CT files are located')	

	
	parser.add_argument('--dose', type=str, nargs=1, required=True,\
				help='Path to RTDOSE dose file')	
	parser.add_argument('--struct', type=str, nargs=1, required=True,\
				help='Path to RTSTRUCT structure file')	
	parser.add_argument('--optional-extra', type=str, nargs=1, required=False,\
				help='Extra text to put in filename of DVH')	
	parser.add_argument('--structures', type=str, nargs="+", required=False,\
				help='List of structures you want to make DVH\'s of. The program will look automatically but might prompt for help.')
	parser.add_argument('-v','--verbose', action='store_true', required=False,\
				help='Supply this flag if you want extra output to the terminal containing a lot of timing information.')
	args = parser.parse_args()
	main()

