#!/usr/bin/env bash
usage() {
    echo "Tool for finding patients that have not been converted yet."
    echo "Script is not general and must be run from a directory"
    echo "containing a list of all paths in which converted patients are"
    echo "in. "
    echo "Usage: $0 [OPTIONS] [path_to_uncompressed_data]"
    echo "Options:"
    echo "  -v, --verbose                           Add this flag to have more verbose output"
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1
}

# Default values for optional arguments
verbose=0

if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -v|--verbose)
            verbose=1
            shift 1
            ;;
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            path+=("$1")
            shift
            ;;
    esac
done





data_dir=${path[0]}
if [[ $verbose -eq 1 ]];
then
    echo " --v: Looking for uncompressed data in ${data_dir}"
    echo " --v: There are $(ls ${data_dir} | wc -l) files in ${data_dir}"
fi

if [[ $verbose -eq 1 ]]; then printf " --v: Finding all done patients ..."; fi
done_patients=$(/home/rasmus/Documents/fluka-recalc-tools/post_hpc/get_converted_patients.sh $(cat patient_directory_list.txt))
if [[ $verbose -eq 1 ]]; then printf " : DONE\n"; fi
for data_p in ${data_dir}/*;
do
    if [[ $verbose -eq 1 ]]; then printf " --v: Checking if %11s has been converted ... " "${data_p##*/}"; fi
	p=$(echo $data_p | sed -e 's/\/\//\//' | rev | cut -d "/" -f1 | rev | cut -d "_" -f1)
	notfound=1
	for c in ${done_patients[@]};
	do
		if [[ $p == $c ]];
		then
			notfound=0
            if [[ $verbose -eq 1 ]]; then printf " : HAS SEEN CONVERSION ATTEMPT\n"; fi
			continue
		fi
	done
	if [[ notfound -eq 0 ]];
	then
		continue
	fi
    if [[ $verbose -eq 1 ]]; then printf " : UNCONVERTED\n"; fi
	echo $p
done
	
		
	
