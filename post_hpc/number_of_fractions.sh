pat=$1

if [[ $pat == "" ]];
then 
	echo "Please supply the patient folder container an allplaninfo.txt file"
	echo "Usage: ./number_of_fractions.sh /path/to/patient/folder"
	echo "Exiting"
fi

fracs=$(cat $pat/allplaninfo.txt | grep fractions | awk '{print $3}' | paste -sd+ | bc);
if [[ $fracs -gt 50 ]]; 
then 
	if [[ $fracs == 78 ]]; 
	then 
		echo "39"; 
	else 
		newfracs=$(cat $pat/allplaninfo.txt|grep fractions|head -1|awk '{print $3}');
		echo "$newfracs"; 
	fi; 
else 
	echo "$fracs"; 
fi ; 
#for fold in */; 
#do 
#	for pat in $fold/*/; 
#	do 
#		fracs=$(cat $pat/allplaninfo.txt | grep fractions | awk '{print $3}' | paste -sd+ | bc);
#		if [[ $fracs -gt 50 ]]; 
#		then 
#			if [[ $fracs == 78 ]]; 
#			then 
#				echo "$pat delivered in 39 fractions **"; 
#			else 
#				newfracs=$(cat $pat/allplaninfo.txt|grep fractions|head -1|awk '{print $3}');
#				echo "$pat delivered in $newfracs fractions ***"; 
#			fi; 
#		else 
#			echo "$pat delivered in $fracs fractions"; 
#		fi ; 
#	done; 
#done
