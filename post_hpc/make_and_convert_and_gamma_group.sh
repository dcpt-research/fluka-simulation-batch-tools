#!/usr/bin/env bash

function usage()
{
    echo "Tool for making a conversion group and automatically converting and calculating gamma passes"
    echo "Usage: $0 [OPTIONS] [conversion_group_name]"
    echo "Options:"
    echo "  --ctvfallback fallback_structure        What structure should we fall back to in case no CTV is found for normalization."
    echo "  --number_of_patients, -n                Max number of patients to include in conversion group (default 10)"
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1

}

# Default values for optional arguments
ctvfallback=""
number_of_patients=10

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        --ctvfallback)
            ctvfallback="$2"
            shift 2
            ;;    
        -n|--number_of_patients)
            number_of_patients="$2"
            shift 2
            ;;    
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            POSITIONAL_ARGS+=("$1")
            shift
            ;;
    esac
done

cg=${POSITIONAL_ARGS[0]}

if [[ $cg == "" ]];
then
    echo "No conversion_group_name supplied."
    usage
fi


mkdir $cg; 
for f in $(ls bnnlis_data/unconverted_data/*/ -d | shuf | head -${number_of_patients}); 
do 
    pname=$(echo $f | cut -d "/" -f3 | cut -d "_" -f1); 
    if [[ ! -d /media/rasmus/3tb_backup/ufhpti_all_data/Archive_unzipped/$pname ]]; 
    then 
        continue; 
    fi; 
    echo "$f -> $pname"; 
    mkdir $cg/${pname}_BNNLIS; 
    ln $f/* $cg/${pname}_BNNLIS;
    mkdir $cg/${pname}; 
    ln /media/rasmus/3tb_backup/ufhpti_all_data/Archive_unzipped/$pname/*.dcm $cg/${pname}/; 
    mv $cg/${pname}_BNNLIS $cg/${pname}; 
    mv $f bnnlis_data/under_processing_data/; 
done; 
if [[ $ctvfallback != "" ]];
then
	/home/rasmus/Documents/fluka-recalc-tools/post_hpc/convert_batch.sh --ctvfallback $ctvfallback $cg/ 
else
	/home/rasmus/Documents/fluka-recalc-tools/post_hpc/convert_batch.sh $cg/ 
fi
/home/rasmus/Documents/fluka-recalc-tools/post_hpc/calc_all_gamma_passes_in_batch.sh $cg/
