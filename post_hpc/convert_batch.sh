#!/usr/bin/env bash

function usage()
{
    echo "Tool for converting all patient folders in a batch. All patient folders must contain a folder with .bnn.lis files"
    echo "and the folder should be named something with \"BNNLIS\", for example \"PATIENT_ID_BNNLIS\""
    echo "Usage: $0 [OPTIONS] [path_to_batch_to_convert]"
    echo "Options:"
    echo "  --ctvfallback fallback_structure        What structure should we fall back to in case no CTV is found for normalization."
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1

}

# Default values for optional arguments
ctvfallback=""

if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        --ctvfallback)
            ctvfallback="$2"
            shift 2
            ;;
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            POSITIONAL_ARGS+=("$1")
            shift
            ;;
    esac
done

if [[ -z ${POSITIONAL_ARGS} ]];
then
    echo "NO POSITIONAL ARGUMENT GIVEN"
    usage
fi








batch=${POSITIONAL_ARGS[0]}


for fold in $batch/*/;
do
	if [[ ! -d ${fold} ]];
	then
		echo "Skipping $fold as it is not a patient directory"
		continue
	fi
	fold_output=$(basename $fold)
	printf "Converting %4s to DICOM ..." "${fold_output}"; 
	bnnlisdir=$(find $fold -maxdepth 1 -type d -name "*BNNLIS*") # finds the folder with the BNNLIS, but has to be in the patient dir
	if [[ $ctvfallback != "" ]];
	then 
		python3 /home/rasmus/Documents/fluka-recalc-tools/post_hpc/automatic_dicom_converter.py --patient $fold --bnnlis $bnnlisdir --silent --ctvfallback ${ctvfallback}
	else
		python3 /home/rasmus/Documents/fluka-recalc-tools/post_hpc/automatic_dicom_converter.py --patient $fold --bnnlis $bnnlisdir --silent 
	fi
    printf " : DONE\n"
done

