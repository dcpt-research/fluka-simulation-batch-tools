#!/usr/bin/env bash
function usage()
{
    echo "Tool for finding the batch id of a given patient only given the output from tar extraction."
    echo "Usage:"
    echo "      ./get_batch_id.sh [OPTION]... [extraction_log_files...]"
    echo "Options:"
    echo "      -p, --patient-id          The id of the patient to find the batch_id for"
    echo "      -n, --suppress-negatives   Suppresses negative findings from output"
    echo "      -h, --help                Print this help message"
    exit 0
}

if [ $# -eq 0 ]; then
    usage
fi

suppress_negatives=0

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -p|--patient-id)
            patient_id="$2"
            shift 2
            ;;
        -n|--suppress-negatives)
            suppress_negatives=1
            shift 1
            ;;
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            POSITIONAL_ARGS+=("$1")
            shift
            ;;
    esac
done

if [[ $patient_id == "" ]];
then
    echo "-p or --patient-id is a required argument."
    usage
fi
for p in ${POSITIONAL_ARGS[@]};
do
    if [[ ! -f $p ]];
    then 
        echo ".tar extraction log file: \"$p\" does not exist."
        usage
    fi
done


for p in ${POSITIONAL_ARGS[@]};
do
    logoutput=$(cat $p | cut -d "/" -f2 | cut -d "_" -f1) # this is the patid for each line
    if [[ $(echo $logoutput | grep -w $patient_id | sort | uniq) != "" ]];
    then
        new_batch_name_0=$(basename $p | cut -d "_" -f4 | cut -d "." -f1)
        new_batch_name_1=$(basename $p | cut -d "_" -f3 | cut -d "." -f1)
        if [[ $new_batch_name_0 != "" ]];
        then
            new_batch_name=$new_batch_name_0
        elif [[ $new_batch_name_1 != "" ]];
        then 
            new_batch_name=$new_batch_name_1
        fi
        output+=($new_batch_name)
    fi
done

if [[ $output == "" ]];
then
    if [[ $suppress_negatives -eq 1 ]];
    then
        exit 0
    fi
    echo "$patient_id not found in any of the supplied batches"
    exit 0
fi

printf "%4d found in : batch_${output[0]}" $patient_id
for out in ${output[@]:1};
do
    printf ", batch_$out"
done
printf "\n"