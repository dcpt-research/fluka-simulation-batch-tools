import os
import sys
import pydicom
import numpy as np
import matplotlib.pyplot as plt

def load_dicom_files(patient_dir):
    rtplan, rtdose, ct_slices = None, None, []
    for root, _, files in os.walk(patient_dir):
        for file in files:
            if file.lower().endswith('.dcm'):
                filepath = os.path.join(root, file)
                ds = pydicom.dcmread(filepath)
                if ds.Modality == 'RTPLAN':
                    rtplan = ds
                elif ds.Modality == 'RTDOSE':
                    if "FLK_Bio-dose" not in file:
                        continue
                    rtdose = ds
                elif ds.Modality == 'CT':
                    ct_slices.append(ds)
    return rtplan, rtdose, ct_slices

def get_isocenter(rtplan):
    if 'BeamSequence' in rtplan:
        sequence = rtplan.BeamSequence
    elif 'IonBeamSequence' in rtplan:
        sequence = rtplan.IonBeamSequence
    else:
        raise ValueError("No BeamSequence or IonBeamSequence found in RTPLAN.")
    
    for beam in sequence:
        if "ControlPointSequence" in beam:
            isocenter = beam.ControlPointSequence[0].IsocenterPosition
        elif "IonControlPointSequence" in beam:
            isocenter = beam.IonControlPointSequence[0].IsocenterPosition
        else:
            raise ValueError("No ControlPointSequence or IonControlPointSequence found in RTPLAN.")
        return isocenter

def get_isocenter_slice(rtdose, isocenter):
    slice_positions = rtdose.ImagePositionPatient[2]
    pixel_spacing = rtdose.PixelSpacing[1]
    slice_thickness = rtdose.SliceThickness
    z_positions = [slice_positions + i * slice_thickness for i in range(rtdose.NumberOfFrames)]
    isocenter_z = isocenter[2]
    isocenter_slice_index = min(range(len(z_positions)), key=lambda i: abs(z_positions[i] - isocenter_z))
    dose_array = rtdose.pixel_array * rtdose.DoseGridScaling
    return dose_array[isocenter_slice_index]

def plot_dose_distribution(dose_slice):
    plt.imshow(dose_slice, cmap='jet', interpolation='nearest')
    plt.colorbar(label='Dose')
    plt.title('Dose Distribution at Isocenter Slice')
    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')
    plt.show()

def main(patient_dir):
    rtplan, rtdose, ct_slices = load_dicom_files(patient_dir)
    if not (rtplan and rtdose):
        print("RTPLAN or RTDOSE not found in the patient directory.")
        return
    try:
        isocenter = get_isocenter(rtplan)
    except ValueError as e:
        print(e)
        return
    dose_slice = get_isocenter_slice(rtdose, isocenter)
    plot_dose_distribution(dose_slice)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python plot_dose.py <patient_directory>")
        sys.exit(1)
    patient_dir = sys.argv[1]
    main(patient_dir)
