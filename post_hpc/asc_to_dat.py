#!/usr/bin/env python3
import argparse

data_x_column = 1

ascfile = "051106_R219Mfull_4_run7_variable_y_z_129.asc"
outputfile = "051106_R219Mfull_4_run7_variable_y_z_129.dat"

xs = []
doses = []


with open(ascfile,'r') as f:
	for line in f:
		realline = line.split()[1:]
		x = realline[data_x_column]
		d = realline[-1]
		xs.append(float(x)/10)
		doses.append(float(d))

maxdose = max(doses)
doses = [d/maxdose for d in doses]

dx = xs[1]-xs[0]

x0 = xs[0]
x1 = xs[-1]

data = []

for i in range(len(xs)):
	left_edge = xs[i]
	if i+1 < len(xs):
		right_edge = xs[i+1]
	else:
		right_edge = xs[i] + (xs[i]-xs[i-1])
	data.append("{:.10E} {:.10E} {:.5E} {:.5E}\n".format(left_edge, right_edge, doses[i], 0))

with open(outputfile,'w') as f:
	f.writelines(data)
