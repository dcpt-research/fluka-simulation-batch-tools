#!/usr/bin/env python3
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

def pad_dvh(dvh,dvhmax):
	
	padded_arr = np.zeros(dvhmax[:,1].shape)
	shape = np.shape(dvh[:,1])
	padded_arr[:shape[0]] = dvh[:,1]
	new_dvh = np.array([dvhmax[:,0],padded_arr[:]]).T
	return new_dvh


args = sys.argv[1::]
bases_r = [a + "/FLUKA_DICOM/dose/" + "Rectum_MC.txt" for a in args]
bases_b = [a + "/FLUKA_DICOM/dose/" + "Bladder_MC.txt" for a in args]

mcn_r = [a+"/"+"variable_RBE-2022-06-22-001/Rectum_mcnamara.txt" for a in args]
wed_r = [a+"/"+"variable_RBE-2022-06-22-001/Rectum_wedenberg.txt" for a in args]
car_r = [a+"/"+"variable_RBE-2022-06-22-001/Rectum_carabe.txt" for a in args]

mcn_b = [a+"/"+"variable_RBE-2022-06-22-001/Bladder_mcnamara.txt" for a in args]
wed_b = [a+"/"+"variable_RBE-2022-06-22-001/Bladder_wedenberg.txt" for a in args]
car_b = [a+"/"+"variable_RBE-2022-06-22-001/Bladder_carabe.txt" for a in args]


dvhs_bas_r = [np.loadtxt(f) for f in bases_r]
dvhs_bas_b = [np.loadtxt(f) for f in bases_b]

dvhs_mcn_r = [np.loadtxt(f) for f in mcn_r]
dvhs_car_r = [np.loadtxt(f) for f in car_r]
dvhs_wed_r = [np.loadtxt(f) for f in wed_r]

dvhs_mcn_b = [np.loadtxt(f) for f in mcn_b]
dvhs_car_b = [np.loadtxt(f) for f in car_b]
dvhs_wed_b = [np.loadtxt(f) for f in wed_b]

v_10_mcn_r = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_mcn_r]
v_10_wed_r = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_wed_r]
v_10_car_r = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_car_r]
v_10_bas_r = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_bas_r]

v_20_mcn_r = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_mcn_r]
v_20_wed_r = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_wed_r]
v_20_car_r = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_car_r]
v_20_bas_r = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_bas_r]


v_30_mcn_r = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_mcn_r]
v_30_wed_r = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_wed_r]
v_30_car_r = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_car_r]
v_30_bas_r = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_bas_r]

v_10_mcn_b = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_mcn_b]
v_10_wed_b = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_wed_b]
v_10_car_b = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_car_b]
v_10_bas_b = [np.interp(10, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_bas_b]

v_20_mcn_b = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_mcn_b]
v_20_wed_b = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_wed_b]
v_20_car_b = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_car_b]
v_20_bas_b = [np.interp(20, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_bas_b]


v_30_mcn_b = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_mcn_b]
v_30_wed_b = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_wed_b]
v_30_car_b = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_car_b]
v_30_bas_b = [np.interp(30, dvhs[:,0], dvhs[:,1]) for dvhs in dvhs_bas_b]


#blads = [v_10_mcn_b, v_20_mcn_b, v_30_mcn_b, v_10_car_b, v_20_car_b, v_30_car_b, v_10_wed_b, v_20_wed_b, v_30_wed_b, v_10_bas_b, v_20_bas_b, v_30_bas_b]
#rects = [v_10_mcn_r, v_20_mcn_r, v_30_mcn_r, v_10_car_r, v_20_car_r, v_30_car_r, v_10_wed_r, v_20_wed_r, v_30_wed_r, v_10_bas_r, v_20_bas_r, v_30_bas_r]

blads = [v_10_bas_b, v_10_mcn_b, v_10_car_b, v_10_wed_b, v_20_bas_b, v_20_mcn_b, v_20_car_b, v_20_wed_b, v_30_bas_b, v_30_mcn_b, v_30_car_b, v_30_wed_b]
rects = [v_10_bas_r, v_10_mcn_r, v_10_car_r, v_10_wed_r, v_20_bas_r, v_20_mcn_r, v_20_car_r, v_20_wed_r, v_30_bas_r, v_30_mcn_r, v_30_car_r, v_30_wed_r]



font = {'family' : 'normal',
	'weight' : 'bold',
	'size'   : 13}

matplotlib.rc('font', **font)

fig,ax = plt.subplots()
ax.boxplot(blads)
#ax.set_xticklabels(["V10Gy_mcn", "V20Gy_mcn", "V30Gy_mcn", "V10Gy_car", "V20Gy_car", "V30Gy_car", "V10Gy_wed", "V20Gy_wed", "V30Gy_wed", "V10Gy_bas", "V20Gy_bas", "V30Gy_bas"],rotation=45)
ax.set_xticklabels([ "V10Gy_bas", "V10Gy_mcn", "V10Gy_car", "V10Gy_wed", "V20Gy_bas", "V20Gy_mcn", "V20Gy_car", "V20Gy_wed", "V30Gy_bas", "V30Gy_mcn", "V30Gy_car", "V30Gy_wed"],rotation=45)
ax.set_ylabel("Volume [%]")
ax.set_title("DVH parameters for bladder - N = {}".format(len(blads[0])))
ax.grid()
plt.tight_layout()
#plt.show()

fig,ax = plt.subplots()
ax.boxplot(rects)
#ax.set_xticklabels(["V10Gy_mcn", "V20Gy_mcn", "V30Gy_mcn", "V10Gy_car", "V20Gy_car", "V30Gy_car", "V10Gy_wed", "V20Gy_wed", "V30Gy_wed", "V10Gy_bas", "V20Gy_bas", "V30Gy_bas"],rotation=45)
ax.set_xticklabels([ "V10Gy_bas", "V10Gy_mcn", "V10Gy_car", "V10Gy_wed", "V20Gy_bas", "V20Gy_mcn", "V20Gy_car", "V20Gy_wed", "V30Gy_bas", "V30Gy_mcn", "V30Gy_car", "V30Gy_wed"],rotation=45)
ax.set_ylabel("Volume [%]")
ax.set_title("DVH parameters for rectum - N = {}".format(len(rects[0])))
ax.grid()
plt.tight_layout()
plt.show()


# Now calculating volume increases for all the VX's

rect_vol_inc = [ [a-b for a,b in zip(v_10_mcn_r,v_10_bas_r)], [a-b for a,b in zip(v_10_car_r,v_10_bas_r)], [a-b for a,b in zip(v_10_wed_r,v_10_bas_r)], [a-b for a,b in zip(v_20_mcn_r,v_20_bas_r)], [a-b for a,b in zip(v_20_car_r,v_20_bas_r)], [a-b for a,b in zip(v_20_wed_r,v_20_bas_r)], [a-b for a,b in zip(v_30_mcn_r,v_30_bas_r)], [a-b for a,b in zip(v_30_car_r,v_30_bas_r)], [a-b for a,b in zip(v_30_wed_r,v_30_bas_r)]]   

blad_vol_inc = [ [a-b for a,b in zip(v_10_mcn_b,v_10_bas_b)], [a-b for a,b in zip(v_10_car_b,v_10_bas_b)], [a-b for a,b in zip(v_10_wed_b,v_10_bas_b)], [a-b for a,b in zip(v_20_mcn_b,v_20_bas_b)], [a-b for a,b in zip(v_20_car_b,v_20_bas_b)], [a-b for a,b in zip(v_20_wed_b,v_20_bas_b)], [a-b for a,b in zip(v_30_mcn_b,v_30_bas_b)], [a-b for a,b in zip(v_30_car_b,v_30_bas_b)], [a-b for a,b in zip(v_30_wed_b,v_30_bas_b)]]   

fig, ax = plt.subplots()
ax.boxplot(rect_vol_inc)
ax.set_xticklabels([ "V10Gy_mcn", "V10Gy_car", "V10Gy_wed",  "V20Gy_mcn", "V20Gy_car", "V20Gy_wed",  "V30Gy_mcn", "V30Gy_car", "V30Gy_wed"],rotation=45)
ax.set_ylabel("Volume increase [%] compared to FLUKA MC (RBE=1.1)")
ax.set_title("DVH volume increases for rectum - N = {}".format(len(rects[0])))
ax.grid()
plt.tight_layout()


fig, ax = plt.subplots()
ax.boxplot(blad_vol_inc)
ax.set_xticklabels([ "V10Gy_mcn", "V10Gy_car", "V10Gy_wed",  "V20Gy_mcn", "V20Gy_car", "V20Gy_wed",  "V30Gy_mcn", "V30Gy_car", "V30Gy_wed"],rotation=45)
ax.set_ylabel("Volume increase [%] compared to FLUKA MC (RBE=1.1)")
ax.set_title("DVH volume increases for bladder - N = {}".format(len(rects[0])))
ax.grid()
plt.tight_layout()
plt.show()
## average dvhs

new_dvhs_mcn_b = [pad_dvh(dvh,dvhs_mcn_b[np.argmax([len(i) for i in dvhs_mcn_b])]) for dvh in dvhs_mcn_b]
new_dvhs_wed_b = [pad_dvh(dvh,dvhs_wed_b[np.argmax([len(i) for i in dvhs_wed_b])]) for dvh in dvhs_wed_b]
new_dvhs_car_b = [pad_dvh(dvh,dvhs_car_b[np.argmax([len(i) for i in dvhs_car_b])]) for dvh in dvhs_car_b]
new_dvhs_bas_b = [pad_dvh(dvh,dvhs_bas_b[np.argmax([len(i) for i in dvhs_bas_b])]) for dvh in dvhs_bas_b]

new_dvhs_mcn_r = [pad_dvh(dvh,dvhs_mcn_r[np.argmax([len(i) for i in dvhs_mcn_r])]) for dvh in dvhs_mcn_r]
new_dvhs_wed_r = [pad_dvh(dvh,dvhs_wed_r[np.argmax([len(i) for i in dvhs_wed_r])]) for dvh in dvhs_wed_r]
new_dvhs_car_r = [pad_dvh(dvh,dvhs_car_r[np.argmax([len(i) for i in dvhs_car_r])]) for dvh in dvhs_car_r]
new_dvhs_bas_r = [pad_dvh(dvh,dvhs_bas_r[np.argmax([len(i) for i in dvhs_bas_r])]) for dvh in dvhs_bas_r]

nom_mcn_b = np.mean(new_dvhs_mcn_b,axis=0)
nom_wed_b = np.mean(new_dvhs_wed_b,axis=0)
nom_car_b = np.mean(new_dvhs_car_b,axis=0)
nom_bas_b = np.mean(new_dvhs_bas_b,axis=0)

max_mcn_b = np.max(new_dvhs_mcn_b,axis=0)
max_wed_b = np.max(new_dvhs_wed_b,axis=0)
max_car_b = np.max(new_dvhs_car_b,axis=0)
max_bas_b = np.max(new_dvhs_bas_b,axis=0)

min_mcn_b = np.min(new_dvhs_mcn_b,axis=0)
min_wed_b = np.min(new_dvhs_wed_b,axis=0)
min_car_b = np.min(new_dvhs_car_b,axis=0)
min_bas_b = np.min(new_dvhs_bas_b,axis=0)

nom_mcn_r = np.mean(new_dvhs_mcn_r,axis=0)
nom_wed_r = np.mean(new_dvhs_wed_r,axis=0)
nom_car_r = np.mean(new_dvhs_car_r,axis=0)
nom_bas_r = np.mean(new_dvhs_bas_r,axis=0)

max_mcn_r = np.max(new_dvhs_mcn_r,axis=0)
max_wed_r = np.max(new_dvhs_wed_r,axis=0)
max_car_r = np.max(new_dvhs_car_r,axis=0)
max_bas_r = np.max(new_dvhs_bas_r,axis=0)

min_mcn_r = np.min(new_dvhs_mcn_r,axis=0)
min_wed_r = np.min(new_dvhs_wed_r,axis=0)
min_car_r = np.min(new_dvhs_car_r,axis=0)
min_bas_r = np.min(new_dvhs_bas_r,axis=0)

# now plot nominal with uncertainty bound from max to min
#


#plt.plot(x, y, 'k-')
#plt.fill_between(x, y-error, y+error)












