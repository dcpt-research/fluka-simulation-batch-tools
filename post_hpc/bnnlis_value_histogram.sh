#!/usr/bin/env bash
function usage()
{
    echo "Tool for making a quick histogram of all values in a .bnn.lis file"
    echo "Plot files will be created in the same directory as the .bnn.lis files"
    echo "unless you provide a plot_directory using -o,--out"
    echo "Usage:"
    echo "$0 [OPTION] [bnnlis_files_to_plot...]"
    echo "OPTIONS:"
    echo "  -h, --help                      Print this help message"
    echo "  -s, --skip=NUM                  Skip NUM lines in the beginning. Defaults to 12."
    echo "  -n, --end=NUM                   Number of lines to include in histogram."
    echo "                                  Defaults to half the number of lines in the file minus 150"
    echo "  -o, --out=OUTPUT_PATH           Base path in which to put the plots."
    exit 1
}

if [ $# -eq 0 ]; then
    usage
fi

# Defalt values
skip=12
end="default"
out="default"

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -s|--skip)
            skip=$2
            shift 2
            ;;
        -n|--end)
            end=$2
            shift 2
            ;;
        -o|--out)
            out=$2
            shift 2
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            bnnlis_files_to_plot+=("$1")
            shift
            ;;
    esac
done

oend=$end
oskip=$skip


if [[ -z ${bnnlis_files_to_plot} ]];
then
    echo "NO POSITIONAL ARGUMENT GIVEN"
    usage
fi

# Checking input
if [ -n "$skip" ] && [ "$skip" -eq "$skip" ] 2>/dev/null; 
then
    if [[ ! $skip -ge 0 ]];
    then 
        echo "-s,--skip argument must be positive"
        usage
    fi
else
    echo "-s,--skip argument must be an integer"
    usage
fi

if [[ $end != "default" ]];
then
    if [ -n "$end" ] && [ "$end" -eq "$end" ] 2>/dev/null; 
    then
        if [[ ! $end -ge 0 ]];
        then 
            echo "-n,--end argument must be positive"
            usage
        fi
    else
        echo "-n,--end argument must be an integer"
        usage
    fi
fi

if [[ $out != "default" ]];
then
    if [[ ! -d $out ]];
    then
        mkdir "$out"
    fi
fi


for b in ${bnnlis_files_to_plot[@]};
do

    plot_out="$(echo "$b" | sed -e 's/\.bnn\.lis/\_hist\.png/')"

    if [[ "$out" != "default" ]];
    then
        plot_out="$out"/"$(basename $plot_out)"
    fi

    tmp_datafile="$(mktemp)"

    if [[ $end == "default" ]];
    then
        end=$(echo "$(cat "$b" | wc -l)/2 - 150" | bc)
    fi

    tail -n +$skip "$b" | head -$end > "$tmp_datafile"

    # Checking if $tmp_datafile only contains the following characters
    # " .E[0-9]-+"
    if [[ $(cat "$tmp_datafile" | grep "[^E0-9 .+-]") != "" ]];
    then
        echo "The selected data in "$b" contains non numeric data."
        echo "The used -s,--skip and -n,--end parameters were:"
        echo "-s,--skip = $oskip"
        echo "-n,--end = $oend"
        usage
    else
        if [[ ! $(cat $tmp_datafile | wc -l) -ge 1 ]];
        then 
            echo "The selected data in "$b" contains no data."
            echo "The used -s,--skip and -n,--end parameters were:"
            echo "-s,--skip = $oskip"
            echo "-n,--end = $oend"
            usage
        fi
    fi
    

    tmp_gnuplot_script=$(mktemp)
    # Write Gnuplot commands to the script
    {
    echo "# Set the number of bins"
    echo "num_bins = 50"
    echo ""
    echo "# Define a function to calculate bin width"
    echo "bin_width(xmin, xmax, num_bins) = (xmax - xmin) / num_bins"
    echo ""
    echo "# Define xmin and xmax"
    echo "stats '$tmp_datafile' using 1 nooutput"
    echo "xmin = STATS_min"
    echo "xmax = STATS_max"
    echo ""
    echo "# Calculate bin width"
    echo "width = bin_width(xmin, xmax, num_bins)"
    echo ""
    echo "# Define the histogram"
    echo "bin(x, width) = width*floor(x/width) + width/2.0"
    echo ""
    echo "# Set histogram style"
    echo "set style data histograms"
    echo "set style histogram rowstacked"
    echo "set style fill solid border -1"
    echo ""
    echo "# Set terminal and output file"
    echo "set terminal pngcairo enhanced font 'Arial,12'"
    echo "set output '$plot_out'"
    echo ""
    echo "set logscale y"
    echo "# Plot the histogram"
    echo "plot '$tmp_datafile' using (bin(\$1, width)):(1.0) smooth freq with boxes title 'Histogram'"
    } > "$tmp_gnuplot_script"

    gnuplot "$tmp_gnuplot_script"


    # Cleanup
    rm $tmp_gnuplot_script
    rm $tmp_datafile
done