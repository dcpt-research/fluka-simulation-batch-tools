#!/usr/bin/env bash

function usage()
{
    echo "Tool for making a group that should be converted"
    echo "The tool is not general. It must reside in a directory with ./get_patients_not_converted_yet.sh and a file" 
    echo "describing where patient data can be found."
    echo "Usage: $0 [OPTIONS] "
    echo "Options:"
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1

}


# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
    esac
done

for p in $(./get_patients_not_converted_yet.sh uncompressed_data/ | shuf | head -10); 
do 
	printf "Setting up conversion for %4s ..." "${p}"
	newdir=/media/rasmus/3tb_backup/simulation_data/patient_data_with_simulation_results/patients_with_unconverted_simulation_data/not_yet_tried_to_convert/conversion_preparation/$p;

	bnnlispath=/media/rasmus/3tb_backup/simulation_data/uncompressed_data/${p}_BNNLIS.tar.gz
	if [[ ! -f /media/rasmus/3tb_backup/simulation_data/uncompressed_data/${p}_BNNLIS.tar.gz ]];
	then
		if [[ -d /media/rasmus/3tb_backup/simulation_data/uncompressed_data/${p}_BNNLIS ]];
		then
			bnnlispath=/media/rasmus/3tb_backup/simulation_data/uncompressed_data/${p}_BNNLIS
		else
			#echo "/media/rasmus/3tb_backup/simulation_data/uncompressed_data/${p}_BNNLIS.tar.gz does not exist!"
			#exit 1
			printf " : ERROR\n"
			continue
		fi
	fi
	if [[ ! -d /media/rasmus/3tb_backup/ufhpti_all_data/Archive_unzipped/${p} ]];
	then
		printf " : ERROR\n"
		continue
		#echo "/media/rasmus/3tb_backup/ufhpti_all_data/Archive_unzipped/${p} does not exist!"
		#exit 1
	fi

	mkdir -p $newdir
	cp -r $bnnlispath $newdir
	cp -r /media/rasmus/3tb_backup/ufhpti_all_data/Archive_unzipped/${p} $newdir
	printf " : DONE\n"
done
