#!/usr/bin/env bash
function help()
{
    echo "Tool for checking validity of .tar.gz, .tar or .targz files."
    echo "Usage:"
    echo "      ./check_targz_files.sh [file_name...]"
}
if [[ $1 == "" || $1 == "-h" || $1 == "--help" ]];
then   
    help
    exit 0
fi
for fl in $@; 
do 
    if [[ ! -f "$fl" ]];
    then
        echo "One or several of the supplied files does not exist."
        echo "At least file: \"$fl\""
        echo "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"
        help
        echo "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"
        echo ""
        echo "Exiting"
        exit 0
    fi
    # Check if file ending is .tar.gz
    command="tar -tzf";
    if [[ $(echo $fl | rev | cut -d "." -f1 | rev) != "gz" ]]; #if not ending in .gz
    then # we check if it ends in .tar
        if [[ $(echo $fl | rev | cut -d "." -f1 | rev) != "tar" ]];
        then
            command="tar -tf";
        elif [[ $(echo $fl | rev | cut -d "." -f1 | rev) != "targz" ]]; # It did not end in either .tar or in .gz. What about .targz
        then
            command="tar -tzf";
        else # not .tar, .gz or .targz
            echo "One of the supplied arguments does not have a file extension of either"
            echo ".tar.gz, .tar og .targz" 
            echo ""
            echo "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"
            help
            echo "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"
            echo "Exiting"
            exit 0
        fi
    fi
done
# Having checked that all files exists and all args are either .tar, .tar.gz or .targz we 
# can proceed
for fl in $@; 
do 
    command="tar -tzf";
    if [[ $(echo $fl | rev | cut -d "." -f1 | rev) == "gz" ]];
    then
        command="tar -tzf";
    fi
    if [[ $(echo $fl | rev | cut -d "." -f1 | rev) == "targz" ]];
    then
        command="tar -tzf";
    fi
    if [[ $(echo $fl | rev | cut -d "." -f1 | rev) == "tar" ]];
    then
        command="tar -tf";
    fi


    printf "Checking %.50s " "${fl} ............................................................"; 

    if $(eval "$command" "$fl" > /dev/null 2>&1); 
    then 
        printf " : OK \n"; 
    else 
        printf " : ERROR, corrupted file \n"; 
    fi; 
done