#!/usr/bin/env python3
import pydicom as pd
import argparse

# prints all structures in a dicom structureset


def printStructures(structure):
	print("There are the following structures available in your RTSTRUCT:")
	for i, name in enumerate([a.ROIName for a in structure.StructureSetROISequence]):
		print("{0:2}: {1}".format(i,name))
def main():
	args = parser.parse_args()
	structpath = args.rtstruct_file[0]
	printStructures(pd.read_file(structpath))
	
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Tool for quick printing of all structures in RTSTRUCT')
	parser.add_argument("rtstruct_file",type=str, nargs=1,\
				help='Path to RTSTRUCT file')	
	args = parser.parse_args()
	main()

