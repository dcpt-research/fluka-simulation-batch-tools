#!/usr/bin/env bash

# Function to display usage information
usage() {
    echo "Usage: $0 [OPTIONS] [list_of_files_to_decompress...]"
    echo "Options:"
    echo "  -j, --max_concurrent_processes VALUE    Max number of unpacking jobs to have running at once."
    echo "  -p, --progress                          Add this flag to have a progress bar for the last decompression"
    echo "                                          in each cycle"
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1
}

# Default values for optional arguments
max_concurrent_processes=4
progressbar=0

if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -j|--max-concurrent-processes)
            max_concurrent_processes="$2"
            shift 2
            ;;
        -p|--progress)
            progressbar=1
            shift 1
            ;;
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            POSITIONAL_ARGS+=("$1")
            shift
            ;;
    esac
done


file_list=${POSITIONAL_ARGS[@]}



i=1

for batch in $file_list
do
    if [[ ! -f $batch && ! -d $batch ]];
    then
        echo "$batch is not a file or directory. Printing --help and exiting."
        usage
    fi
done


for batch in $file_list
do 
    command=""
    if [[ $(echo $batch | rev | cut -d "." -f1 | rev) == "gz" && $(echo $batch | rev | cut -d "." -f2 | rev) == "tar" ]];
    then # .tar.gz case
        command="tar -xvzf"
    fi
    if [[ $(echo $batch | rev | cut -d "." -f1 | rev) == "tar" ]];
    then # .tar case
        command="tar -xvf"
    fi
    if [[ $(echo $batch | rev | cut -d "." -f1 | rev) == "targz" ]];
    then # .targz case
        command="tar -xvzf"
    fi
    if [[ $(echo $batch | rev | cut -d "." -f1 | rev) == "zip" ]];
    then # .targz case
        command="unzip"
    fi
    if [[ $command == "" ]];
    then 
        echo "Input argument $batch is not a compressed archive. Skipping."
        continue
    fi

    full_command="$command $batch > ${batch}.log"

    if [[ $i -eq $max_concurrent_processes ]]
    then 
        echo "Uncompressing $batch"
        i=1
        if [[ $progressbar != 1 ]];
        then
            eval "$full_command" &
        elif [[ $progressbar == 1 ]];
        then
            pv $batch | tar -xzv > ${batch}.log &
        fi
        pids[${i}]=$!
        for pid in ${pids[*]};
        do
            wait $pid
        done
        unset pids
        continue
    elif [[ $i -lt $max_concurrent_processes ]]
    then 
        echo "Uncompressing $batch"
        i=$((i+1))

        eval "$full_command" &
        pids[${i}]=$!
    fi
done
