#!/usr/bin/env bash
function usage()
{
    echo "Tool for setting up and converting a conversion group following use of \"make_conversion_group.sh\""
    echo "Usage: $0 [OPTIONS] [path_to_batch_to_convert]"
    echo "Options:"
    echo "  --ctvfallback fallback_structure        What structure should we fall back to in case no CTV is found for normalization."
    echo "  --remove-uncompressed-data              Supply this flag to remove all directories that were uncompressed after conversion."
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1

}

# Default values for optional arguments
ctvfallback=""
remove_uncompressed_data=0

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        --ctvfallback)
            ctvfallback="$2"
            shift 2
            ;;    
        --remove-uncompressed-data)
            remove_uncompressed_data=1
            shift 1
            ;;   
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            POSITIONAL_ARGS+=("$1")
            shift
            ;;
    esac
done

batch=${POSITIONAL_ARGS[0]}
for f in $batch/*; 
do 
    patname="${f##*/}"
    printf "Setting up %4s ..." "$patname"; 
    if [[ ! -d ${f}/${patname}_BNNLIS ]]; 
    then 
        if [[ ! -f ${f}/${patname}_BNNLIS.tar.gz ]]; 
        then printf " : ERROR\n"; 
            continue; 
        fi; 
        tar -xf ${f}/${patname}_BNNLIS.tar.gz -C ${f}; 
        touch ${f}/uncompressed_tar.tmp
    fi; 
    mv ${f}/${patname}/* ${f}; 
    rmdir ${f}/${patname}; 
    printf " : DONE\n"; 
done

options=""
if [[ ${ctvfallback} != "" ]];
then
    options="--ctvfallback ${ctvfallback}"
fi

eval "$(dirname $0)/convert_batch.sh $options ${batch}"

if [[ ${remove_uncompressed_data} -eq 1 ]];
then
    for f in ${batch}/*;
    do
        patname="${f##*/}"
        printf "Removing the data we uncompressed for ${patname} ..."
        if [[ -f ${f}/uncompressed_tar.tmp ]];
        then
            rm -rf ${f}/${patname}_BNNLIS
        fi
        printf " : DONE\n"
    done
fi
rm -rf $batch/*/uncompressed_tar.tmp

