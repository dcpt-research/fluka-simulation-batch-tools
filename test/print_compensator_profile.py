import pydicom as pd
import numpy as np
import argparse

def check_valid_rtplan_file(rtplan, beamname=None, *args, **kwargs):
    if rtplan.Modality != "RTPLAN":
        raise ValueError("Input argument --rtplan has modality: {} and not RTPLAN".format(rtplan.Modality))
    
    if "IonBeamSequence" not in rtplan:
        raise ValueError("Input RTPLAN DICOM file has no IonBeamSequence")

    ibss = rtplan.IonBeamSequence
    found_IonRangeCompensatorSequence = False
    found_beamname = True if beamname == None else False
    for ibs in ibss:
        if "IonRangeCompensatorSequence" in ibs:
            found_IonRangeCompensatorSequence = True
        if ibs.BeamName == beamname:
            found_beamname = True
            if not "IonRangeCompensatorSequence" in ibs:
                raise ValueError("Input RTPLAN DICOM file IonBeamSequence with BEAMNAME: {} has no IonRangeCompensatorSequence".format(beamname))
    if not found_IonRangeCompensatorSequence:
        raise ValueError("Input RTPLAN DICOM file has no IonRangeCompensatorSequences")
    if not found_beamname:
        raise ValueError("Input RTPLAN DICOM file has no IonBeamSequence with the supplied BEAMNAME: {}".format(beamname))
    

    
def compensator_sequence(rtplan, beamname=None, *args, **kwargs):
    ircs = rtplan.IonBeamSequence[0].IonRangeCompensatorSequence[0]
    if beamname != None:
        for ibs in rtplan.IonBeamSequence:
            if ibs.BeamName == beamname:
                ircs = ibs.IonRangeCompensatorSequence[0]
    thicknesses = ircs.CompensatorThicknessData
    mainCylThick = np.max(thicknesses)
    drill_depths = mainCylThick - thicknesses
    nrows =  ircs.CompensatorRows
    ncols =  ircs.CompensatorColumns
    x0,y0 =  ircs.CompensatorPosition
    dx, dy = ircs.CompensatorPixelSpacing
    depth_matrix = np.reshape(drill_depths, (nrows, ncols))
    drillHoleDiameter = ircs.CompensatorMillingToolDiameter

    output = []
    for i, row in enumerate(depth_matrix):
        if 0 == len(row[row>0]):
            nrows = nrows - 1
            continue

        index_0      = np.where(row>0)[0][0]
        index_minus1 = np.where(row>0)[0][-1]
        ni = index_minus1 - index_0 + 1
        deltaxi = dx
        x0i = x0 + index_0 * deltaxi
        y0i = y0 - i * dy
        nonZero_depths = row[index_0:index_minus1+1]
        line0 = "{} {} {} {}\n".format(ni, deltaxi, x0i, y0i)
        line1 = " ".join(["{}".format(l) for l in nonZero_depths])
        line1 += "\n"
        output.append(line0)
        output.append(line1)
    
    metadata = []
    metadata.append("{}\n".format(nrows))
    metadata.append("{}\n".format(mainCylThick))
    metadata.append("{}\n".format(drillHoleDiameter))
    for o in output:
        metadata.append(o)

    return metadata


def main():
    args = parser.parse_args()

    rtplan_path = args.rtplan
    beamname = args.beamname
    ds = pd.read_file(rtplan_path)


    check_valid_rtplan_file(ds, beamname=beamname)
    compensator_sequence_out = compensator_sequence(ds, beamname=beamname)

    if not args.output == None:
        with open(args.output, "w") as f:
            f.writelines(compensator_sequence_out)
            exit()
    for line in compensator_sequence_out:
        print(line)
    exit()


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Tool for writing the compensator sequence to commonly used formats, i.e. for TOPAS simulation. \
                                     If several are found, the first one if printed except if --beamname is supplied, then the one corresponding\
                                     to BEAMNAME will be used')
    parser.add_argument('rtplan', type=str, \
                help='Path to RTPLAN DICOM file containing a IonRangeCompensatorSequence')       
    parser.add_argument('--beamname', type=str, \
                help='Beamname for which to output the Compensator sequence')      
    parser.add_argument('-o','--output', type=str, required=False,\
                help='The compensator file will be written to this file. If not supplied the program will print it.')
        
    args = parser.parse_args()
    main()
