#!/bin/bash

## replaces the most common FLUKA particle ID's with Particle Data Group Particle ID's.

file=$1

if [[ $file == "" ]];
then
	echo "Input a file for which I should replace FLUKA ID's with Particle Data Group ID's"
	exit 0
fi

sed -i 's/13$/211/' "$file"
sed -i 's/-3$/1000010020/' "$file"
sed -i 's/3$/11/' "$file"
sed -i 's/-4$/1000010030/' "$file"
sed -i 's/-5$/1000020030/' "$file"
sed -i 's/-6$/1000020040/' "$file"
sed -i 's/4$/-11/' "$file"
sed -i 's/7$/22/' "$file"
sed -i 's/8$/2112/' "$file"
sed -i 's/1$/2212/' "$file"
