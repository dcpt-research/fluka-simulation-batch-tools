# Fluka simulation batch tools
These scripts are to be used in conjunction with FRES scripts from UiB (Universitetet i Bergen / Bergen University)

## Requirements:
In general, these scripts use python, bash and numerous python libraries as well as commandline applications. They are written on linux, 
and tested using debian under kUbuntu 20.04. Use `Python 3`, and generally assume the scripts to use any of the following modules. 
### Python modules:
`matplotlib`
`numpy`
`code`
`pydicom`
`readline`
`rlcompleter`
`argparse`
`pexpect`


All the modules can be installed using

`python3 -m pip install matplotlib numpy code pydicom readline rlcompleter argparse pexpect`

## Commandline applications
`plastimatch`
`dcmtk`

as well as regular commandline programs, such as `sed`, `awk` and so on.

`dcmtk` and `plastimatch` can be installed using:

`sudo apt install dcmtk plastimatch`

`python3 -m pip install pyplastimatch`

NB: Do note that currently plastimatch can not be installed through default repositories on ubuntu 22.xx based systems.

### fluka-to-pdg-particle-codes.sh
A simple bash script to convert some common FLUKA particles to particle data group (PDG) codes (found at [https://pdg.lbl.gov/2021/reviews/rpp2020-rev-monte-carlo-numbering.pdf](https://pdg.lbl.gov/2021/reviews/rpp2020-rev-monte-carlo-numbering.pdf)).

Usage: 
`./fluka-to-pdg-particle-codes.sh <file>`

NB: The code simply runs some `sed -i` replacements on the input file so be careful not to run it one something you did not mean to.

It is meant to make FLUKA phase-space logs compatible with other MC codes, such as TOPAS or GEANT4.

## Scripts in pre_hpc

are to be used in preparation of putting patient simulations on a HPC (high performance computing) system,
in this case one running SLURM.


FLUKA voxel files should be generated with a CT DICOM series where everything but the patient is set to HU=-1000
(at least in the case of UFHPTI Eclipse treatment plans).

### allplaninfo.py
This program parses the important information from the RTPLAN DICOM files in the patient directory.
It uses `pydicom` python module. This can be installed using `python3 -m pip install pydicom`.

The program outputs directly to `stdout`.

Usage: `python3 allplaninfo.py <patient directory> > <patient directory>/allplaninfo.txt`


### auto_sort_dicoms_ds.py
This script is a wrapper for a FRES (Bergen group set of scripts) script which transforms a folder of only DICOM's and an `allplaninfo.txt`
(created using `allplaninfo.py`) into a folder with DICOM's and a FLUKA simulation environment such that the treatment recalculation can be performed.

Usage:
`python3 auto_sort_dicoms_ds.py --patient <patient directory> --histories 1000000 --FRES <path to FRES repository> --silent`

where `<patient directory>` is the directory with DICOM files and an `allplaninfo.txt` file with beam parameters,

`--histories 1000000` indicate how many primary particles the simulation should start with - in this case 1 million

and `--silent` makes the program not output anything into the terminal. You can remove this if there are errors to get a better idea.

### sort_batch.sh
This is a script which calls `auto_sort_dicoms_ds.py` in the correct manner on an entire batch of patients.
It assumes you want to set up simulation with 1 million primaries per simulation.

Usage: `./sort_batch.sh <batch directory>`


### set_HU.sh
This script creates a duplicate of an all `.dcm` files of modality CT in a folder, where all voxels outside the structure 
body are set to -1000. They are placed in a seperate folder inside the patient folder.

This script uses `plastimatch` and `dcmtk`. They can be installed on a debian system by

`sudo apt install plastimatch dcmtk`

Usage:
`./set_HU.sh <patient directory>`

### set_HU_batch.sh
This script runs set_HU.sh for an entire batch. This is the easiest way to set HU values for a batch.
This script uses `plastimatch` and `dcmtk`. They can be installed on a debian system by

`sudo apt install plastimatch dcmtk`

Usage:
`./set_HU_batch.sh <batch directory>`

### prepare_batch.sh 
Duplicates a batch of patient files into one containing only the necessary information to conduct a simulation.
(no DICOM  information, only voxel files, source files and input files - as well as geometry).

A batch in this case is just a bunch of patients, for example it could be a batch of 10 patients.

Usage:
`./prepare_batch.sh <batch directory>`

### set_voxel_files.sh
Sets the correct line in the FLUKA input files to point to the voxel file for the given patient.

More importantly it sets the correct amount of spacing such that FLUKA doesn't get angry at you.

This script is automatically run when you run `prepare_batch.sh`

Usage:
`./set_voxel_files.sh <patient_dir>`


## Scripts in hpc

are to be used on the SLURM cluster.

The most important scripts are:

`setup_batch.sh`

it basically sets up the batch for simulation and creates appropriate jobscript files for both simulation and merging.
It also sets up at submit_batch.sh file in the batch folder so you just run this one to start everything.
When this is done you have a results folder with a folder containing all the .bnn.lis files.

### setup_batch.sh
This script is the wrapper for all other scripts. This script calls `setup_jobscript.sh` on all patients in a batch.

`setup_jobscript.sh` copies 
`run_all.sh`, 
`run_single_simulation.sh`, 
`start_merge.sh`, 
`sim_then_merge.sh`, 
`general_datamergin_jobscript.sh`, 
`general_jobscript.sh`
as well as other scripts into the necessary folders of the simulation environment.

Most important is probably `general_jobscript.sh` and `general_datamergin_jobscript.sh` as these are the actual 
jobscripts submitted to the queuing system.

`general_jobscript.sh` will launch the simulation.

`general_datamergin_jobscript.sh` will launch the datamerging when the simulation is done.


## Scripts in post_hpc

### Automatic_dicom_converter.py

This script can be run with 

`python3 automatic_dicom_converter.py --patient <patient directory> --bnnlis <fluka scoring directory> --fres <path to FRES scripts>`

It also supports `--help` or `-h` for a list of possible options.

`python3 automatic_dicom_converter.py --help`

### progress_gammaplot.py

This script requires all folders to have gamma pass `.txt` files, created from `gamma_pass.sh` located in `cli-dose-comparison`.

This script is run with `python3 progress_gammaplot.py` in the directory of the patient folders.

The script makes some plots for all the patient's gamma pass values, on 1mm / 1%, 2mm / 2% and 3mm / 3% criteria.

`cli-dose-comparison` is found at [cli-dose-comparison](https://gitlab.com/dcpt-research/cli-dose-comparison)

### makedvh.py

This script creates a `.txt` file with a dose volume histogram (DVH) for two input files of type RTDOSE and RTSTRUCT.

The script will prompt you for which structure you wish to make a dvh for.

Usage:

`python3 makedvh.py -i <ctdir> -d <RTDOSE path> -s <RTSTRUCT path> -o "extra text to append to the output file name"`

The output file will be named "structure_extra text to append to the output file name.txt" where structure is the structure you choose in the interactive prompt.

### dvh_comp.py

This script compares all dvh's found in all given patient directories. The directories need to have a dvh files in them, created by `makedvh.py`

Currently the script just finds one of specific names - this should ideally be changed to a more general approach.

Usage:
`python3 dvh_comp.py <patient dir 1> <patient dir 2> .... <patient dir N>`
