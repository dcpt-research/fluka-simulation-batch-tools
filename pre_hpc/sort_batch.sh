batch=$1
if [[ $batch == "" || $batch == "--help" || $batch == "-h" ]];
then
	echo "You should supply the base path the batch you are looking to sort using FRES sort_dicoms_ds.py script and"
	echo "auto_sort_dicoms.py script in fluka-recalc-tools"
	echo "Exiting."
	exit 0
fi
for fold in ${batch}/*;
do
	echo "Sorting $fold"
	fold_fullpath=$(realpath "${fold}")
	python3 /home/rasmus/Documents/fluka-recalc-tools/pre_hpc/auto_sort_dicoms_ds.py --patient "${fold_fullpath}" --histories 1000000 --silent
done
