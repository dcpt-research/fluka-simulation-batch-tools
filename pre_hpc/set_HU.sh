#!/bin/bash
plastimatch=plastimatch

if [[ $1 == "" ]];
then
	echo "You need to specify the patient folder in which to set all houndfield units"
	echo "outside the patient structure to -1000."
	echo "Usage: ./set_HU.sh /path/to/patient/directory"
	echo ""
	echo "Exiting"
	exit 0
fi
if [[ $1 == "-h" ]];
then
	echo "You need to specify the patient folder in which to set all houndfield units"
	echo "outside the patient structure to -1000."
	echo "Usage: ./set_HU.sh /path/to/patient/directory"
	echo ""
	echo "Exiting"
	exit 0
fi


pat_fold="$1"
str=""
dos=""
dump=$(dcmdump +sd "${pat_fold}"/*.dcm | tr -d '\000')
strsopInUID=$(echo "$dump" | grep RTSTRUCT -B10 | grep SOPInstanceUID | cut -d "[" -f2 | cut -d "]" -f1)
str=$(find "${pat_fold}" -name "*${strsopInUID}.dcm")
dossopInUID=$(echo "$dump" | grep RTDOSE -B10 | grep SOPInstanceUID | cut -d "[" -f2 | cut -d "]" -f1)
dossopInUID=$(echo "$dossopInUID" | cut -d " " -f1)
dos=$(find "${pat_fold}" -name "*${dossopInUID}.dcm")

${plastimatch} convert --input "$str" --output-prefix "${pat_fold}"/masks --prefix-format nrrd --output-ss-list "${pat_fold}"/masks/image.txt --input-dose-img "$dos" >> "${pat_fold}"/mask_creation_output.txt

mkdir -p "${pat_fold}"/cts
for file in "${pat_fold}"/*.dcm;
do
	if [[ $(dcmdump "$file" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1) == "CT" ]];
	then
		mv "$file" "${pat_fold}"/cts
	fi
done
mkdir -p "${pat_fold}"/CTs_for_FLUKA_voxel_outside_BODY_-1000
${plastimatch} mask --input "${pat_fold}"/cts --output-format dicom --output "${pat_fold}"/CTs_for_FLUKA_voxel_outside_BODY_-1000 --mask-value -1000 --mask "${pat_fold}"/masks/[b,B][o,O][d,D][y,Y]*.nrrd >> "${pat_fold}"/mask_output.txt

mv "${pat_fold}"/cts/* "${pat_fold}"
rm -rf "${pat_fold}"/cts
