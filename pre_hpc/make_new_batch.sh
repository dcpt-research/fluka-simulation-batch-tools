#!/bin/bash

if [[ $1 == "" || $1 == "-h" || $1 == "--help" ]]; #|| $2 =="" || $3 == "" || $4 == "" ]];
then
	echo "This script makes a new batch which you name."
	echo "It creates a folder for the corresponding batch"
	echo "and copies files to it."
	echo "Please do not use spaces, but if you do \"quote it so it is 1 string\"."
	echo ""
	echo "	Usage:"
	echo "		./make_new_batch.sh [OPTION] [new_batch_path]"
	echo "  OPTIONS:"
	echo "          -h, --help          Print this help message"
	echo "WARNING: This script is not so general so only works when in this folder..."
	exit 0
fi
min=6
if [[ $(cat not_simulated_yet_TR3_B8.txt | wc -l) -lt $min ]];
then
	echo "There are not $min unsimulated cases"
	echo "Exiting"
	exit 0
fi

mkdir -p "$1"
for i in $(cat not_simulated_yet_TR3_B8.txt | shuf | tail -$min); 
do 
	cp -r /media/rasmus/3tb_backup/ufhpti_all_data/Archive_unzipped/$i "$1" 
	echo $i >> already_completed.txt
done
rm not_simulated_yet_TR3_B8.txt -f
for i in $(cat all_B8.txt | sort | uniq | shuf); 
do 
	if [[ $(cat already_completed.txt | grep -w $i) == "" ]]; 
	then 
		echo "$i" >> not_simulated_yet_TR3_B8.txt; 
	fi; 
done
