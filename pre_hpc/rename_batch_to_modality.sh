#!/usr/bin/env bash
batch=$1


if [[ $batch == "" ]];
then
	echo "Please provide the correct batch as standard input."
	echo "The program will add the modality of all dicom files to the"
	echo "beginning of their name"
	echo "Usage: ./rename_batch_to_modality.sh /path/to/batch"
	exit 0
fi
if [[ $batch == "-h" ]];
then
	echo "Please provide the correct batch as standard input."
	echo "The program will add the modality of all dicom files to the"
	echo "beginning of their name"
	echo "Usage: ./rename_batch_to_modality.sh /path/to/batch"
	exit 0
fi


batch=$(realpath "$batch")
for fold in $batch/[0-9]*/; 
do 
    echo "Starting on ${fold}" && 
    cd "${fold}" && 
    for file in *.dcm; 
    do 
        if [[ $(echo "$file" | grep "_") == "" ]];
        then
            mv "$file" "$(dcmdump "$file" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)_$file"; 
        fi
    done && 
    cd $batch ;
done
