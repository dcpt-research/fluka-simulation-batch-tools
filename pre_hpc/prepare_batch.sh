#!/usr/bin/env bash
batch=$1
if [[ $batch == "" || $batch == "-h" || $batch == "--help" ]]; 
then
	echo "Tool for preparing only files necessary for simulation on supercomputer cluster."
	echo "End batch contains no DICOM files and no identifyable parameters."
	echo "You need to specify which batch you are trying to prepare!"
	echo "	Usage:"
	echo "		./prepare_batch.sh /path/to/batch"
	exit 0
fi

bp=$(realpath $batch)
preppath="$(realpath $batch)/.."
gren_bat=$preppath/${bp##*/}_grendel
mkdir -p $gren_bat
for fold in $batch/*;
do
	patnum=${fold##*/}
	mkdir -p ${gren_bat}/${fold##*/}
	for plan in $(find ${fold} -type d -name "FLUKA*");#)${fold}/FLUKA*;
	do
		onlyplan=${plan##*/}
		#echo "the last character in: >${plan}< is: >${plan: -1}<"
		mkdir -p ${gren_bat}/${patnum}/data/$(echo $onlyplan | cut -d "_" -f1)_${onlyplan: -1}
		thisnewplanpath=${gren_bat}/${patnum}/data/$(echo $onlyplan | cut -d "_" -f1)_${onlyplan: -1}
		cp ${plan}/${patnum}.vxl ${thisnewplanpath}
		cp ${plan}/fluscw_IFT.f ${thisnewplanpath}
		cp -r ${plan}/Components ${thisnewplanpath}
		cp -r ${plan}/PSG_* ${thisnewplanpath}
		for fieldfold in $(dirname $(find ${plan} -name "source.f"));
		do
			fieldname=${fieldfold##*/}
			mkdir -p ${thisnewplanpath}/${fieldname}
			cp ${fieldfold}/*.inp ${thisnewplanpath}/${fieldname}/
			cp ${fieldfold}/*.dat ${thisnewplanpath}/${fieldname}/
			cp ${fieldfold}/*.f ${thisnewplanpath}/${fieldname}/
		done
		
	done
	
done
for fold in ${gren_bat}/[0-9]*;
do
	/home/rasmus/Documents/fluka-recalc-tools/pre_hpc/set_voxel_files.sh "${fold}"
done
