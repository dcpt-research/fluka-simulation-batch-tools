import numpy as np


hu_min = -1000
data = []
for ct in cts:
    ds = pd.read_file(ct, force = True)
    arr = ds.pixel_array
    arr2 = np.uint16(ds.rescaleslope * arr + ds.rescaleintercept - hu_min)
    data.append(str(arr2.data))
    del ds

























# Do the real processing --- this step sets up the voxelfile
self.dicom()._writeVoxelInit(self.voxelfile["text"]) # self.voxelfile["text"] is the voxel filename


# writes all the slices
for i,(l,fn) in enumerate(self.dicom()._slices):
				# Show progress
				self.dicom()._writeVoxelSlice(fn,i)

# writes the last part with materials, corrfacts and so on
if not self.dicom()._writeVoxelEnd():
			self.flair.notify("DICOM to Voxel",
				"Error writing Voxel file",
				tkFlair.NOTIFY_ERROR)
			return




def _writeVoxelInit(self, filename): ## self in this context is Dicom class
    try: self._f = open(filename,"wb")
    except: return False

    # voxel data
    self._data = []

    # initialize the rtstuct arrays
    self._roiname   = {}	# index=ROINumber, value=name,volume
    self._roicomb   = {}	# index=:RTSTRUCT[:RTSTRUCT] (Unique combination) value=sequential index
    self._roidata   = []	# matrix of seq-index
    self._roicolor  = {}	# color of each structure
    self._roiplanar = {}	# planar information for CLOSED_PLANAR

    self._tdraw = 0.0
    self._tproc = 0.0
    return True

def startVoxels(dicom, voxelfilename):
    


def _writeVoxelSlice(self, filename, s=None, update=None):
		if s is not None and not self.sliceFrom <= s <= self.sliceTo: return
		dataset = dicom.read_file(filename, force=True)
		if self.slice:
			self.__writeVoxelSliceData(dataset.pixel_array)
		else:
			for s,data_slice in enumerate(dataset.pixel_array):
				if s is None or self.sliceFrom <= s <= self.sliceTo:
					self.__writeVoxelSliceData(data_slice)
				if update and update(s): break
		del dataset


def _writeVoxelEnd(self):
    # write title
    self._write(self.title)

    # memory size
    header = struct.pack("=5i", self.columns(), self.rows(), self.slices(),
                    len(self.reg2hu)-1, len(self.kreg))
    if len(self._roicomb)>1:
        # Number of structures (consecutive)
        header += struct.pack("=i", max(self._roiname.keys()))

        # Number of combinations
        header += struct.pack("=i", len(self._roicomb)-1)	# Minus the 0 index

        # Maximum length of combinations
        maxcomb = max([x.count(":") for x in self._roicomb.keys()])
        header += struct.pack("=i", maxcomb)	# Minus the 0 index

        # Number of CLOSED_PLANAR structures
        header += struct.pack("=i", len(self._roiplanar))

    self._write(header)

    # voxel dimension
    self._writeStruct("=3d", 0.1*self.dx, 0.1*self.dy, 0.1*self.dz)

    # data
    self._write(string.join(self._data,""))
    del self._data

    # hu -> region matrix
    self._writeStruct("=%dH"%(len(self.kreg)), *self.kreg)

    # rt struct
    self.__writeVoxelRTstruct()

    # embed material definition
    self.writeMaterials()

    # assignmat matrix
    self.writeAssignmats()

    # correction factors
    self.writeCorrFactors()

    self._f.close()
    return True