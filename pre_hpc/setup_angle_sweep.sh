#!/bin/bash
if [[ $5 == "" || $1 == "-h" || $1 == "--help" ]];
then
	echo "Sets up simulations for making start angle calibration."
	echo "Usage:"
	echo "./setup_angle_sweep.sh /path/to/target/folder start_angle end_angle step_angle number_of_histories_per_simulation"
	exit 0
fi
target_fold=$1
angle_start=$2
angle_end=$3
angle_step=$4
hists=$5
for ang in $(seq $angle_start $angle_step $angle_end);
do
	for fold in B7-*;
	do
		cp -r $fold $target_fold/${fold}$ang
		mv $target_fold/${fold}$ang/$(echo $fold | sed -e 's/_angle_//').inp $target_fold/${fold}$ang/${fold}${ang}.inp
	done
	padded_ang=$(printf "%.3f" $ang)
	padded_hists=$(printf "%10d" $hists)
	find $target_fold -name "B7*.inp" -exec sed -i 's/RM6_ROT/'"${padded_ang}"'\./' {} \;
	find $target_fold -name "B7*.inp" -exec sed -i 's/HHHHHHHHHH/'"$padded_hists"'/' {} \;
	
done
cp -r running_files/* $target_fold
