#!/bin/bash
bat=$1
if [[ $bat == "" || $bat == "-h" || $bat == "--help" ]];
then
	echo "This scipt will open the FLUKA flair interface on each sorted patient directory in the supplied batch folder"
	echo "Usage:"
	echo "      ./make_vxl_files_batch.sh /path/to/batch"
	exit 0
fi
for pat in $bat/*; 
do 
	echo "$pat"; flair -s $(find $pat -name "*[0-9]*.inp" | head -1); 
done
mkdir $bat/../$(basename $bat)_vxls
mv $bat/*/*/*.vxl $bat/../$(basename $bat)_vxls
for pat in $bat/*;
do
	patname=$(echo $pat | rev | cut -d "/" -f1 | rev)
	echo $patname
	for fluk in $pat/FLUKA*PLAN*;
	do
		cp $bat/../$(basename $bat)_vxls/${patname}.vxl $fluk;
	done
done
rm -rf $bat/../$(basename $bat)_vxls
