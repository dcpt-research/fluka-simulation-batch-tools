import pydicom
import os,sys
def get_range(option_nr, lollipops):

    lollipops = lollipops[::-1] # reverses lollipops to get correct range
                                # they go from large to small in DICOM

    coefficients = [
        [-0.666, 0.3547, 0],
        [-0.666, 0.3547, 0],
        [-0.7049, 0.2557, -0.000000000000002],
        [-0.8185, 0.1171, 0.0118],
        [-2.0781, 0.1614, 0.0019],
        [-5.8852, 0.541, -0.0092],
        [-14.239, 1.072, -0.0166],
        [-26.636, 2.0281, -0.0356],
    ]
    coeffs = coefficients[option_nr-1]
    sorted_lollipops = [9, 6, 2, 5, 3, 4]
    sorted_thick = [0.0341, 0.0681, 0.1419, 0.2838, 0.5675, 1.135]

    FSreal = 0
    for i in range(6):
        FSreal += lollipops[i] * sorted_thick[i]

    # FSreal ~= coeffs[0] + coeffs[1] * rangepat + coeffs[2] * rangepat**2 +- FS_error
    c0, c1, c2 = coeffs[:3]
    r1 = (-c1 + ( c1**2 - 4*c2*(c0 - FSreal - 0.005*FSreal) )**0.5 ) / (2*c2)
    r2 = (-c1 - ( c1**2 - 4*c2*(c0 - FSreal) )**0.5 ) / (2*c2)

    #Seems for B8 the answer is always r1

    return r1


def get_modwidth(option_nr,stop_digit):
    stop_angle = stop_digit * 45 / 32
    coefficient_list = [
        [77.293, 27.299, -2.2171, 0.3471, -0.0266],
        [69.721, 39.743, -7.3176, 1.0324, -0.0404],
        [117.08, 12.13, 3.561, -0.6774, 0.0354],
        [100.99, 44.736, -5.4091, 0.3803, -0.0102],
        [101.62, 28.946, -1.9777, 0.0674, -0.0004],
        [135.87, 20.37, 0.0629, -0.0977, 0.0036],
        [129.56, 35.802, -2.6111, 0.0814, -0.0007],
        [135.91, 27.634, -1.67, 0.039439, -0.0002436]
    ]
    coeffs = coefficient_list[option_nr-1]

    c0,c1,c2,c3,c4 = coeffs[:5]


    # 0 = (c0-stop_angle) + c1*mw + c2*mw**2 + c3*mw**3 + c4*mw**4
    e = c0-stop_angle
    d,c,b,a = c1,c2,c3,c4
    
    p1 = 2*c**3 - 9*b*c*d + 27*a*d**2 + 27*b**2*e - 72*a*c*e
    p2 = p1 + (-4*(c**2 - 3*b*d + 12*a*e)**3 +p1**2 )**0.5
    p3 = (c**2 - 3*b*d + 12*a*e ) / ( 3*a*(p2/2)**(1.0/3) ) + (p2/2)**(1.0/3) / (3*a)
    p4 = ( b**2/(4*a**2) - 2*c/(3*a) + p3 )**0.5
    p5 = b**2 / (2*a**2) - 4*c / (3*a) - p3
    p6 = ( -b**3/a**3 + 4*b*c/a**2 - 8*d/a ) / (4*p4)

    #mw1 = -b/4/a - p4/2 - (p5-p6)**0.5/2
    #mw2 = -b/4/a - p4/2 + (p5-p6)**0.5/2
    mw3 = -b/4/a + p4/2 - (p5+p6)**0.5/2
    mw4 = -b/4/a + p4/2 + (p5+p6)**0.5/2

    # apparently always mw3 is correct, even though equation has 2 solutions
    return mw3
def main(argv):
    if len(argv) > 0:
        RTdir = argv[0]
    if RTdir[-1] != "/":
        RTdir+="/"
    for filename in os.listdir(RTdir):
        if filename[-4:] != ".dcm":
            continue
        ds = pydicom.read_file(RTdir+filename)
        if ds[0x8,0x60].value != "RTPLAN":
            continue

        print("\n___***___***___***___***___***___***___***___\n")
        print("Succesfully read DICOM RTPLAN:", filename)

        for ii in range(len(ds[0x300a,0x03a2].value)):
            beam_name = ds[0x300a,0x03a2][ii][0x300a,0x00c2].value
            beam_dose = "Beam dose could not be found automatically in RTPLAN"
            try:
                beam_dose = ds[0x300a,0x0070][0][0x300c,0x0004][0][0x300a,0x0084].value
            except KeyError:
                pass
            option_nr = ds[0x300a, 0x03a2][ii][0x300a, 0x0342][0][0x300a,0x034c].value
            option_nr = int(option_nr.split("_")[1][1])
            stop_angle = ds[0x300a,0x03a2][ii][0x300a, 0x03a8][0][0x300a,0x380][0][0x300a,0x384].value
            lollipop = ds[0x300a, 0x03a2][ii][0x300a, 0x03a8][0][0x300a, 0x0370]
            lollipop1 = ds[0x300a, 0x03a2][ii][0x300a, 0x03a8][0][0x300a, 0x0370][0][0x300a, 0x0372].value
            lollipop2 = ds[0x300a, 0x03a2][ii][0x300a, 0x03a8][0][0x300a, 0x0370][1][0x300a, 0x0372].value
            if len(lollipop1) != 6:
                lollipop1 = lollipop2
            lollipops = lollipop1
            lollipops = [int(i) for i in lollipops]

            sorted_lollipops = [9, 6, 2, 5, 3, 4]
            lollipops_nums = []
    
            for i in range(6):
                if lollipops[5-i]: # lollipops are ordered big to small, and thus must be reversed
                    lollipops_nums.append(sorted_lollipops[i])
    
            noFractions = int(ds[0x300a,0x0070][0][0x300a,0x0078].value)
            try: 
                targPresDose = float(ds[0x300a,0x0010][1][0x300a,0x0026].value)
            except Exception:
                try:
                    targPresDose = float(ds["DoseReferenceSequence"][0]["TargetPrescriptionDose"].value)
                except Exception:
                    targPresDose = 0
            muPerFraction = (targPresDose)/noFractions*90.90909090
            muTotal = (targPresDose)*90.90909090
            beamEnergy = float(ds[0x300a,0x03a2][ii][0x300a,0x03a8][0][0x300a,0x0114].value)
    
    
            print("BEAM NAME: ",beam_name)
            print("Beamdose in GY: ", beam_dose)
            print("Beam energy: ", beamEnergy)
            print("Total Target prescription dose: ", targPresDose)
            print("Delivered in ",noFractions," fractions")
            print("Monitor units per fraction assuming conversion factor of 1: ", muPerFraction)
            print("Monitor units in total assuming conversion factor of 1: ", muTotal)
            print("Option number is: B"+str(option_nr))
            print("Stop digit is: ",stop_angle)
            print("Stop angle is: ",stop_angle*45/32)
            print("FS (lollipop) settings: ",lollipops)
            print("FS (lollipop) numbers: ",lollipops_nums)
            print("\nRANGE: ",get_range(option_nr,lollipops))
            print("MODULATION WIDTH: ",get_modwidth(option_nr,stop_angle))
    

if __name__=="__main__":
    if "-h" in sys.argv or "--help" in sys.argv:
        print("This script gives you all relevant information of the DICOM RTPLAN for simulation")
        print("Usage:")
        print("    python3 allPlanInfo.py /path/to/patient/folder")
        exit()
    if(len(sys.argv) > 1):
        main(sys.argv[1:])
