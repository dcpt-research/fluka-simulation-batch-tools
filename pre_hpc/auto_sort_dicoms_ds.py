## this program will seek to call FRES program "sort_dicoms_ds.py" automagically
## with arguments from allplaninfo.txt
import argparse
import pexpect
import os
import code
import readline
import rlcompleter
import re
import time

def main():
	args = parser.parse_args()
	fres_path = ""
	if args.FRES:
		fres_path = args.FRES[0]
	else:
		if os.getenv("FRES"):
			fres_path = os.getenv("FRES")
		else: 
			# we neither have the shell variable or the supplied path.
			# Get mad, throw a fit - make the user know they fucked up
			print("You are supposed to either have a environment variable named FRES")
			print("with the path to the FRES repository or supply the repository path")
			print("using --FRES /path/to/FRES")
			print("Thank you for understanding.")
			exit()
	patient_dir = args.patient[0]
	try:
		histories = args.histories[0]
	except TypeError:
		histories = args.histories
	silentmode = args.silent
	if not "allplaninfo.txt" in os.listdir(patient_dir):
		print("You should have a file called allplaninfo.txt which was made with allplaninfo.py in the patient directory.")
		print("Exiting")
		exit(0)
	planinfo_dict = parse_allplaninfo(patient_dir+"/allplaninfo.txt")


	### we should probably make a while loop, such that we keep feeding information while it is still asking
	### something like
	### while child.isalive()
		### run through an entire process
		### expect plan selection
		### ...
		### send ranges etc
		### ...
		### ...
		### override plan? no.
		### back to beginning. if it was the last iteration then it 
		### will stop automatically since the child will die

	#print("python3 {}/sort_dicoms_ds.py --histories {}".format(fres_path ,histories))
	child = pexpect.spawn("python3 {}/sort_dicoms_ds.py --histories {}".format(fres_path ,histories))

	
	### child expect patient directory
	if not silentmode: print("Expecting to provide path.")
	child.expect("Provide the directory of the DICOM files")
	### child send patient directory
	if not silentmode: print("Providing path.")
	child.sendline("{}".format(patient_dir))
	
	plan_or_range = -1
	keep_going = True
	have_waited_for_plan = False
	have_waited_for_range = False
	while keep_going:
		### child expect "there are x plans available"
		if not silentmode: print("Expecting to be asked to select a plan.")
		if not have_waited_for_plan and not have_waited_for_range: child.expect("There are +\d +plans") ## sometimes the spaces around the number of plans is repeated
		have_waited_for_plan = False
		### child send <enter>	("")
		if not have_waited_for_range: 
			if not silentmode: print("Choosing default plan.") 
			child.sendline("")

		### plan_or_range = child expect [there are x plans, provide range]
		if not silentmode: print("Either expecting to be asked for another plan, or to provide the range for a field in this plan.")
		if not have_waited_for_range: plan_or_range = child.expect(["There are +\d +plans", "Provide the range for field"]) ## sometimes the spaces around the number of plans is repeated
		### if plan_or_range == 1 -- we were asked for which plan
		if plan_or_range == 0:
			if not silentmode: print("We were asked to select a plan. Selecting default.")
			### child send <enter>
			child.sendline("")
			if not silentmode: print("Expecting to be asked to provide a range for a field.")
			### child expect "provide range"
			child.expect("Provide the range for field")
			
			
		
		### at this point we know we are ready for provide range
		### but we need to check which field it is asking for



		fieldname = str(child.buffer).split("\"")[1] # this will return the field name
		if not silentmode: print("Providing the field range of {} g/cm^2 for field {}.".format(planinfo_dict[fieldname]["range"], fieldname))
		child.sendline("{}".format(planinfo_dict[fieldname]["range"]))
		have_waited_for_range = False

		if not silentmode: print("Expecting to be asked to provide a modulation width for a field.")
		child.expect("Provide the modulation width for field")
		if not silentmode: print("Providing the field modulation width of {} g/cm^2 for field {}."\
			.format(planinfo_dict[fieldname]["modulation_width"], fieldname))
		child.sendline("{}".format(planinfo_dict[fieldname]["modulation_width"]))

		if not silentmode: print("Expecting to be asked to provide monitor units per fraction.")
		child.expect("Provide the monitor units for field")
		if not silentmode: print("Providing {} MU for field {}.".format(planinfo_dict[fieldname]["monitor_units"], fieldname))
		child.sendline("{}".format(planinfo_dict[fieldname]["monitor_units"]))

		if not silentmode: print("Expecting to be asked to provide the output factor.")
		child.expect("Provide the output factor for field")
		if not silentmode: print("Sending an output factor of 1.")	
		child.sendline("1")

		if not silentmode: print("Expecting to be asked if the dose per fraction is okay.")
		child.expect("Prescription fraction dose calculated to be")
		if not silentmode: print("Sending an enter signal to say yes, this is fine.")
		child.sendline("")

		
#		vars = globals()       
#		vars.update(locals())
#		readline.set_completer(rlcompleter.Completer(vars).complete) 
#		readline.parse_and_bind("tab: complete")                     
#		code.InteractiveConsole(vars).interact()	
	
		keep_going_or_not = child.expect(["Working...","There are +\d +plans", "Provide the range for field"])
		if keep_going_or_not == 0:
			keep_going = False
		if keep_going_or_not == 1:
			have_waited_for_plan = True
		if keep_going_or_not == 2:
			have_waited_for_range = True
		plan_or_range = -1


	maxtime = 60  # wait for a maximum of 60 seconds for processes to conclude
	t0 = time.time()
	t1 = time.time()
	while child.isalive() and t1-t0 <= maxtime:
		t1 = time.time()


	# vars = globals()       
	# vars.update(locals())
	# readline.set_completer(rlcompleter.Completer(vars).complete) 
	# readline.parse_and_bind("tab: complete")                     
	# code.InteractiveConsole(vars).interact()	
	

def parse_allplaninfo(path_to_allplaninfo):
	# reads the allplaninfo and returns a dictionary like:
	# dict[fieldname][field_property]
	# where fieldname is like LPO, 1A1_RPO or whatever the fieldname found is.
	# and field_properties are like: Beamdose, beam_energy, number_fractions, target_dose, monitor_units, monitor_units_total,
	# range, modulation_width, option_nr
	dict = {}
	thisfield = ""
	with open(path_to_allplaninfo, "r") as f:
		for line in f:
			firstpart = line
			lastpart = ""
			if ":" in line: 
				firstpart, lastpart = line.split(":")
			lastpart = lastpart.replace("\n","").replace("\r","")

			if firstpart == "BEAM NAME":
				thisfield = lastpart.lstrip().rstrip().replace(" ","_")
				dict[thisfield] = {}
			if firstpart == "Total Target prescription dose":
				dict[thisfield]["target_dose"] = float( lastpart )
			if re.search("Delivered in +\d+ +fractions", line):
				dict[thisfield]["number_fractions"] = int(re.findall("\d+",line)[0])
			if firstpart == "Monitor units per fraction assuming conversion factor of 1":
				dict[thisfield]["monitor_units"] = float(lastpart)
			if firstpart == "Monitor units in total assuming conversion factor of 1":
				dict[thisfield]["monitor_units_total"] = float(lastpart)
			if firstpart == "Option number is":
				dict[thisfield]["option_nr"] = lastpart
			if firstpart == "RANGE":
				dict[thisfield]["range"] = float(lastpart)
			if firstpart == "MODULATION WIDTH":
				dict[thisfield]["modulation_width"] = float(lastpart)
	return dict
			

if __name__=="__main__":
	parser = argparse.ArgumentParser(description='Tool for automagically sorting unsorted DICOMS and preparing them for FLUKA FRES simulation. ')
	parser.add_argument('--patient', type=str, nargs=1, required=True,\
				help='Path to patient directory containing DICOM CTs, RTDOSEs, RTSTRUCT and RTPLANs.')	
	parser.add_argument('--histories', type=int, nargs=1, required=False, default=1000000,\
				help='How many histories should the simulation be set up for. Default is 1000000.')	
	parser.add_argument('--FRES', type=str, nargs=1, required=False,\
				help='Path to FRES repository. Either specify here or "export FRES=/path/to/FRES".')
	parser.add_argument('--silent', action='store_true', required=False,\
				help='Supply this flag if you want no output to the terminal')
	args = parser.parse_args()
	main()
