#!/bin/bash
batch=$1
if [[ $batch == "" || $batch == "--help" || $batch == "-h" ]];
then
	echo "This script will rename the DICOM files to their modality and prepare the allplaninfo.txt file in "
	echo "the directories. It will also make files with HU=-1000 outside the patient"
	echo "You should supply the base path the batch you are looking to sort using FRES sort_dicoms_ds.py script and"
	echo "auto_sort_dicoms.py script in fluka-recalc-tools"
	echo "Exiting."
	exit 0
fi
echo "Renaming batch"
/home/rasmus/Documents/fluka-recalc-tools/pre_hpc/rename_batch_to_modality.sh "$batch"
echo "Setting HU values"
/home/rasmus/Documents/fluka-recalc-tools/pre_hpc/set_HU_batch.sh "$batch"
echo "Preparing allplaninfo for all patients."
for pat in $batch/*;
do
	python3 /home/rasmus/Documents/fluka-recalc-tools/pre_hpc/allPlanInfo.py "$pat" > "$pat"/allplaninfo.txt
done
