#!/bin/bash
targ=$1
t=$(realpath $targ)
pat_num=${t##*/}
for fold in $(find $targ -name "source.f");
do
	field_fold=$(dirname $fold)
	inp_file=$(find $field_fold -name "*.inp")
	sed -i 's@VOXELS           0.0       0.0       0.0    PATROT*@VOXELS           0.0       0.0       0.0    PATROT                    \.\.\/'"${pat_num}"'@' $inp_file
done
