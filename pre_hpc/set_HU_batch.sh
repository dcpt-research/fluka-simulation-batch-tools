bat=$1
if [[ $bat == "-h" || $bat == "--help" || $bat == "" ]];
then
	echo "You need to specify the batch folder with patient folders in which to set all houndfield units"
        echo "outside the patient structure to -1000."
        echo "Usage: ./set_HU.sh /path/to/batch/"
        echo ""
        echo "Exiting"
        exit 0
fi
for pat in $bat/*;
do
	/home/rasmus/Documents/fluka-recalc-tools/pre_hpc/set_HU.sh "$pat"
done
