import pydicom as pd
import argparse
import readline                                              
import rlcompleter
import code
import os
import time


def code_interact(globals, locals):
	vars = globals
	vars.update(locals)
	readline.set_completer(rlcompleter.Completer(vars).complete)
	readline.parse_and_bind("tab: complete")
	code.InteractiveConsole(vars).interact()	
def main():
	args = parser.parse_args()
	t0 = time.time()
	dicom_type = ""
	path_type = "file"
	path = ""
	verbose = args.verbose
	# log = args.log
	find_all =args.all
	if args.dose:
		dicom_type="RTDOSE"
		path = args.dose
	if args.struct:
		dicom_type="RTSTRUCT"
		path = args.struct
	if args.plan:
		dicom_type="RTPLAN"
		path = args.plan
	if args.ct:
		dicom_type="CT"
		path = args.ct
	tag = args.tag[0]

	path=path[0]
	t1 = time.time()

	if os.path.isdir(path):
		path_type="directory"
	
	if path[-1] != "/" and path_type == "directory":
		path = path + "/"

	if (path[-4:].lower() != ".dcm" and path_type == "file") or not os.path.isfile(path):
		path = os.path.dirname(path)+"/"
		path_type = "directory"

	t2 = time.time()
	if log: print("main: From t0 to t1: {:.4f}".format(t1-t0))
	if log: print("main: From t1 to t2: {:.4f}".format(t2-t1))

	if path_type == "directory":
		t3 = time.time()
		result = find_tag_in_directory(path, tag, dicom_type, find_all=find_all)
		if not verbose:
			print(result)
		if verbose and not find_all:
			print("{} from file: {}".format(result[0],result[1]))
		if verbose and find_all:
			print("{} from file: {}".format(result[:-1],result[-1]))
		t4 = time.time()
		if log: print("main: From t3 to t4: {:.4f}".format(t4-t3))

		exit()

	if path_type == "file":
		t5 = time.time()
		if find_tag_in_file(path,"Modality")[0] == dicom_type:
			result = find_tag_in_file(path, tag)
			if not verbose:
				print(result[0])
			if verbose:
				print("{} from file: {}".format(result[0], result[1]))
			t6 = time.time()
			if log: print("main: From t5 to t6: {:.4f}".format(t6-t5))
			exit()
		else:
			t7 = time.time()
			result = find_tag_in_directory(os.path.dirname(path)+"/", tag, dicom_type, find_all=find_all)
			if not verbose:
				if find_all:
					print(result)
			if verbose and not find_all:
				print("{} from file: {}".format(result[0],result[1]))
			if verbose and find_all:
				print("{} from file: {}".format(result[:-1],result[-1]))
			t8 = time.time()
			if log: print("main: From t7 to t8: {:.4f}".format(t8-t7))
			exit()
	# code_interact(globals(), locals())

def find_tag_in_directory(path, tag, dicom_type, find_all=False):
	t0 = time.time()
	if not find_all:
		for file in os.listdir(path):
			if file[-4:].lower() != ".dcm":
				continue
			if pd.read_file(path+file).Modality == dicom_type:
				t1 = time.time()
				if log: print("find_tag_in_directory: From t0 to t1: {:.4f}".format(t1-t0))
				return find_tag_in_file(path+file,tag)
	else:
		t2 = time.time()
		results = []
		for file in os.listdir(path):
			if file[-4:].lower() != ".dcm":
				continue
			if pd.read_file(path+file).Modality == dicom_type:
				results.append(find_tag_in_file(path+file,tag)[0])
		t3 = time.time()
		if log: print("find_tag_in_directory: From t2 to t3: {:.4f}".format(t3-t2))
		return results


def find_tag_in_sequence(sequence, tag):
	t0 = time.time()
	for d in sequence:
		if type(d.value) is pd.sequence.Sequence:
			for seq in d: 
				res = find_tag_in_sequence(seq,tag)
				if res:
					t1 = time.time()
					if log: print("find_tag_in_sequence: From t0 to t1: {:.4f}".format(t1-t0))
					return res
		if d.keyword.lower() == tag.lower().replace(" ",""):
			return d.value



def find_tag_in_file(file_path, tag):
	t0 = time.time()
	ds = pd.read_file(file_path)
	final_value = find_tag_in_sequence(ds,tag)
	t1 = time.time()
	if log: print("find_tag_in_file: From t0 to t1: {:.4f}".format(t1-t0))
	return (final_value, file_path)


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Tool for quickly identifying a DICOM tag in a patient folder. The tag can be in a CT, RTPLAN, RTSTRUCT or RTDOSE\
			DICOM file.')
	# parser.add_argument('-i', type=str, nargs=1, required=True,\
	# 			help='Directory where CT files are located')	

	group1 = parser.add_mutually_exclusive_group(required=True)
	group1.add_argument('--dose', type=str, nargs=1, required=False,\
				help='Use if looking for an RTDOSE DICOM tag. Specify a file if the RTDOSE file is known in the patient directory, otherwise specify patient directory.')	
	group1.add_argument('--struct', type=str, nargs=1, required=False,\
				help='Use if looking for an RTDOSE DICOM tag. Specify a file if the RTSTRUCT file is known in the patient directory, otherwise specify patient directory.')	
	group1.add_argument('--plan', type=str, nargs=1, required=False,\
				help='Use if looking for an RTDOSE DICOM tag. Specify a file if the RTPLAN file is known in the patient directory, otherwise specify patient directory.')	
	group1.add_argument('--ct', type=str, nargs=1, required=False,\
				help='Use if looking for an RTDOSE DICOM tag. Specify a file if the ct DICOM file is known in the patient directory, otherwise specify patient directory.\
				The program will choose the first CT in the folder, so dont look for slice specific stuff.')	

	parser.add_argument('--tag', type=str, nargs=1, required=True,\
				help='The name of the DICOM tag you are looking for. All possible capitalizations will be searched.')


	group2 = parser.add_mutually_exclusive_group(required=False)
	group2.add_argument('-v','--verbose', action='store_true', required=False,\
				help='Supply this flag if you want extra output to the terminal containing extra information including file name of the file with the final information.')
	
	group2.add_argument('-a','--all', action='store_true', required=False,\
				help='Supply this flag if you want to find all instances of a flag. They will be comma seperated.')

	parser.add_argument('--log', action='store_true', required=False,\
				help='Timing information at different points in the code. Only for development purposes.')

	args = parser.parse_args()
	log = args.log
	main()