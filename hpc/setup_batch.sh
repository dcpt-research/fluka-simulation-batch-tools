#!/usr/bin/env bash
usage() {
    echo "Tool for setting up batch scripts for slurm simulation"
    echo "Only works for simulation batches setup using the other scripts in this repository."
    echo "Usage: $0 [OPTIONS] [path_to_batches...]"
    echo "Options:"
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1
}


if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            batch_list+=("$1")
            shift
            ;;
    esac
done

for batch in ${batch_list[@]};
do
    echo "Setting up simualations for $batch ....."
    if [[ ! -d $batch ]];
    then 
        echo "PATH NOT FOUND"
        continue
    fi
    if [[ ! -f /home/raskli/scripts/hpc/setup_jobscript.sh ]];
    then
        echo " : /home/raskli/scripts/hpc/setup_jobscript.sh NOT FOUND"
        exit 1
    fi
    if [[ ! -f /home/raskli/scripts/hpc/submit_batch.sh ]];
    then
        echo " : /home/raskli/scripts/hpc/submit_batch.sh NOT FOUND"
        exit 1
    fi
    for fold in ${batch}/*;
    do
        if [[ ! -d $fold ]]
        then
            echo " : $fold is not a simulation directory!"
            continue
        fi
        /home/raskli/scripts/hpc/setup_jobscript.sh ${fold}
	/home/raskli/scripts/hpc/ensure_voxel_geometry_set.sh ${fold}
    done
    cp "/home/raskli/scripts/hpc/submit_batch.sh" $batch
    echo "DONE"
done
