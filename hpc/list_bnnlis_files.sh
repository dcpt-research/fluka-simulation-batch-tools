#!/usr/bin/env bash


function usage()
{
    echo "Tool for finding the sizes of all .bnn.lis files and sort by size."
    echo "The tool looks in all files under your $PWD that are .bnn.lis"
    echo "The list is always sorted by file size, useful for checking for empty files"
    echo "Usage:"
    echo "$0 [OPTION] "
    echo "OPTIONS:"
    echo "  -h, --help                      Print this help message"
    exit 1
}



# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
    esac
done

ls $(find . -name "*.bnn.lis") -lh | sort -k 5 -hr
