#/bin/bash
if [[ $1 == "-h" ]];
then
	echo "Call this script to stop all submitted slurm jobs by your user";
	exit 0;
fi;
alljobs=$(mj | awk '{print $1}' | tail -n +3);
for id in $alljobs; 
do 
	scancel $id; 
done
