bat=$1
origfold=$(pwd)
cd $origfold
i=1
job_ids=()
for fold in $(find $bat -maxdepth 1 -type d | tail -n +2);
do
	cd $fold
	echo "Submitted job: $fold"
	job_id=$(sbatch run_simulation_jobscript.sh | awk '{print $4}')
	job_ids+=("$job_id")
	cd $origfold
done

total_number_of_array_jobs=$(cat $fold/run_simulation_jobscript.sh | grep "\-\-array" | cut -d "%" -f1 | rev | cut -d "-" -f1 | rev)

for job_id in "${job_ids[@]}"; do
	for arr_id in $(seq 1 1 $total_number_of_array_jobs);
	do
		scontrol wait "${job_id}_${arr_id}"
	done
done

if [[ $(find -maxdepth 2 -type d -name "results") == "" ]];
then
	echo "No results have been produced!! Exiting without merging."
	exit 1
fi


i=1
for fold in $(find $bat -maxdepth 1 -type d | tail -n +2);
do
	cd $fold
	echo "Merging job: $fold"
	./start_merge_then_wait.sh &
	pidsm[${i}]=$!
	i=$((i+1))
	cd $origfold
done
for pid in ${pidsm[*]};
do
	wait $pid
done



