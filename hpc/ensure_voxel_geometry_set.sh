#!/usr/bin/env bash

# Replaces "-APTCYLO" with "-APTCYLO -VOXEL" if "VOXEL" is not in the 
# "Environment_regions.geo" files in the patient dir


usage() {
    echo "Tool for ensuring the VOXEL region is set to not be in the VOID region"
    echo "of the FLUKA simulation."
    echo "Only works for simulation batches setup using the other scripts in this repository."
    echo "Usage: $0 [OPTIONS] [path_to_patients...]"
    echo "Options:"
    echo "  -n, --dry-run                           Dry run. Prints the proposed changes"
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1
}

# default values for args
sed_opt="-i "
sed_print=""
dry_run=0

if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -n|--dry-run)
            sed_opt="-n "
            sed_print="p"
            dry_run=1
            shift
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            pat_list+=("$1")
            shift
            ;;
    esac
done

for pat in ${pat_list[@]};
do
	if [[ ! -d $pat ]];
	then
		echo "Patient directory \"$pat\" does not exist. Exiting."
		usage
	fi
	for geofile in $(find "$pat" -name "Environment_regions.geo");
	do
		# Check if file contains "VOXEL"
		if [[ $(cat "$geofile" | grep "VOXEL") == "" ]];
		then
			if [[ $dry_run -eq 1 ]];
			then
				printf "Replacing: \n"
				sed $sed_opt 's/-APTCYLO/-APTCYLO/g'${sed_print}'' "$geofile"
			fi
			if [[ $dry_run -eq 1 ]];
			then
				printf "With     : \n"
			fi
			sed $sed_opt 's/-APTCYLO/-APTCYLO -VOXEL/g'${sed_print}'' "$geofile"
		fi
	done
	
done
