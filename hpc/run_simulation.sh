#! /bin/bash

spawns=$1
cycles=$2

name=${PWD##*/}
Ninp=$1
ext='.inp'
fullname=$name$ext
row=$(grep -n RANDOMIZ ${fullname} | cut -d : -f 1)
end=$(grep -c "."  ${fullname})
let row=row-1
let end=end
let end=end-row-1
cat  ${fullname} | head -n ${row} > first
cat  ${fullname} | tail -n ${end} > last
for (( i=1; i<="$Ninp"; i++ )); do
 Nrand="$i"
     echo  "RANDOMIZ          1.       $Nrand. " >> .RAND$i.txt
     cat first .RAND$i.txt last > "$name"_"$i".inp
     rm .RAND$i.txt
done
rm first last
echo ""
echo "INPUT $name has been replicated $Ninp TIME(S)"
echo ""

for (( i=1 ; i<="$spawns"; i++ )); do
     $FLUPRO/flutil/ldpmqmd -o ${PWD##*/}_exe_$i source.f ../fluscw_IFT.f
done
wait
for (( i=1 ; i<="$spawns"; i++ )); do
     nohup $FLUPRO/flutil/rfluka -e ${PWD##*/}_exe_$i -N0 -M"$cycles" ${PWD##*/}_$i &
done
