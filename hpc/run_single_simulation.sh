#! /bin/bash

RNDSEED=$1

spawns=1
cycles=1

name=${PWD##*/}
Ninp=1
ext='.inp'
fullname=$name$ext
row=$(grep -n RANDOMIZ ${fullname} | cut -d : -f 1)
end=$(grep -c "."  ${fullname})
let row=row-1
let end=end
let end=end-row-1
cat  ${fullname} | head -n ${row} > first
cat  ${fullname} | tail -n ${end} > last
for (( i=1; i<="$Ninp"; i++ )); do
	RND="$RNDSEED"
	echo  "RANDOMIZ          1.    $RND. " >> .RAND$i.txt
	cat first .RAND$i.txt last > "$name"_"$i".inp
	rm .RAND$i.txt
done
rm first last
echo ""
echo "INPUT $name has been replicated $Ninp TIME(S)"
echo ""

$FLUPRO/flutil/ldpmqmd -o ${PWD##*/}_exe_1 source.f ../fluscw_IFT.f
wait
$FLUPRO/flutil/rfluka -e ${PWD##*/}_exe_1 -N0 -M"$cycles" ${PWD##*/}_1
