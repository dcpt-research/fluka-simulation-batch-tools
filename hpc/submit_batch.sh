bat=$1
origfold=$(pwd)
cd $origfold
for fold in $(find $bat -maxdepth 1 -type d | tail -n +2);
do
	cd $fold
	./sim_then_merge.sh
	cd $origfold
done
