#! /bin/bash

FIELDNO=$1
RNDSEED=$2

cd FLUKA_$FIELDNO

i=0
for d in ${PWD}/*/
do
	#cd "$d" && test -r run_single_simulation.sh && echo "rndseed = "$(echo "$RNDSEED+$FIELDNO+$i" | bc)""
	(cd "$d" && test -r run_single_simulation.sh && ./run_single_simulation.sh "$(echo "$RNDSEED+$i" | bc)" ) &
	i=$((i+1))
	pids[${i}]=$!
done
for pid in ${pids[*]};
do
	wait $pid
done
