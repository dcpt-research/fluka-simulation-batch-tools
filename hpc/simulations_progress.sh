#!/usr/bin/env bash


# Function to display usage information
usage() {
    echo "Tool for finding the progress of a slurm array job."
    echo "Prints the lowest simulation count in the batch."
    echo "Usage: $0 [OPTIONS] [path_to_batches...]"
    echo "Options:"
    echo "  -n, --array-iterations                  Number of array iterations of each simulation run in the batch. Default 200."
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1
}

# Default values for optional arguments
array_iterations=200

if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -n|--array-iterations)
            array_iterations="$2"
            shift 2
            ;;
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            POSITIONAL_ARGS+=("$1")
            shift
            ;;
    esac
done


file_list=${POSITIONAL_ARGS[@]}
minimum=0
for batch in ${file_list[@]};
do
    batchname=$(basename $batch)
    printf "Progress of $batchname is: "
    calculate_minimum=1
    for f in $batch/*/;
    do
        if ! ls $f/*.out 1> /dev/null 2>&1;
        then
            calculate_minimum=0
        fi
    done
    if [[ -d $batch && $calculate_minimum -eq 1 ]];
    then 
	minimum=$(for f in $batch/*/; do ls $f/*_*.out | rev | cut -d "/" -f1 | rev |  cut -d "_" -f2 | cut -d "." -f1 | xargs printf "%03d\n" | sort | tail -1 ; done | sort | head -1)
    fi
    printf "%03d / %03d\n" "$(( 10#$minimum ))" "$(( 10#$array_iterations ))"
    minimum=0
done
