#!/usr/bin/env bash
usage() {
    echo "Tool for finding the number of files within all subfolders of some paths"
    echo "Usage: $0 [OPTIONS] [paths...]"
    echo "Options:"
    echo "  -l, --lustre-fs                         Add this flag to see files under your home directory (~) "
    echo "                                          on a Lustre based file system"
    echo "  -s, --sub-directories                   Add this flag to see a directory by directory breakdown"
    echo "  -h, --help                              Display this help message"
    echo ""
    exit 1
}

# Default values for optional arguments
sub_directories=0
lustre_fs=0

if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -s|--sub-directories)
            sub_directories=1
            shift 1
            ;;
        -l|--lustre-fs)
            lustre_fs=1
            shift 1
            ;;
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            path+=("$1")
            shift
            ;;
    esac
done


if [[ ${lustre_fs} -eq 1 ]];
then
    lfs quota -h ~
    exit 0
fi

paths_to_look_in=${path[@]}

for p in ${paths_to_look_in[@]};
do
    echo "File count under \"${p}\""
    command="du --inodes -s \"${p}\""
    if [[ sub_directories -eq 1 ]];
    then
        for ll in ${p}/*;
        do
            l=$(basename $ll)
            echo "    $(eval "${command}/${l}")"
        done
    fi
    echo "    $(eval "${command}" | sed -e 's/\.//') In total"
done
