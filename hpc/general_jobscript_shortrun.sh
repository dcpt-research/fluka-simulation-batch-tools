#!/bin/bash
#SBATCH --job-name=JOBNAME
#SBATCH --partition=q12,q16,q20,q24,q28,q36,q40,q48
#SBATCH --mem=4G
#SBATCH --cpus-per-task=NUMBEROFFIELDS
#SBATCH --time=00:05:00
#SBATCH --export=NONE
#SBATCH --array=1-200%50


echo "========= Job started  at `date` =========="

echo "My jobid: $SLURM_JOB_ID"
echo "My array id: $SLURM_ARRAY_TASK_ID"

ml load gcc/9.3.0

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK:-1}
export FX=gfortran-9.3

NFIELDS=NUMBEROFFIELDS


cd $SLURM_SUBMIT_DIR
mkdir -p /scratch/${SLURM_JOB_ID}/${SLURM_ARRAY_TASK_ID}
cp -r data /scratch/${SLURM_JOB_ID}/${SLURM_ARRAY_TASK_ID}/
cd /scratch/${SLURM_JOB_ID}/${SLURM_ARRAY_TASK_ID}/data

for i in $(seq 1 1 $NFIELDS); do 
	# runs the field simulation number $i with random seed $SLURM_ARRAY_TASK_ID
	# and saves the PID so we can wait for the process to finish
	./run_all.sh $i $(echo "$SLURM_ARRAY_TASK_ID * $NFIELDS * 4 + (4*$i)" | bc) &
	pids[${i}]=$!
done
# wait for PIDS to finish
for pid in ${pids[*]}; do
	wait $pid
done
# now all spawned subprocesses are done and we can copy home the data we made
for fold in ./*; do
	for fielddir in $(find $fold -name "source.f"); do
		dir=$(dirname $fielddir)
		mkdir -p ${SLURM_SUBMIT_DIR}/results/${dir}/${SLURM_ARRAY_TASK_ID}
		cp ${dir}/*_fort* ${SLURM_SUBMIT_DIR}/results/${dir}/${SLURM_ARRAY_TASK_ID}
		cp ${dir}/*.out   ${SLURM_SUBMIT_DIR}/results/${dir}/${SLURM_ARRAY_TASK_ID}
		cp ${dir}/*.err   ${SLURM_SUBMIT_DIR}/results/${dir}/${SLURM_ARRAY_TASK_ID}
		cp ${dir}/*.log   ${SLURM_SUBMIT_DIR}/results/${dir}/${SLURM_ARRAY_TASK_ID}
		cp ${dir}/ran*    ${SLURM_SUBMIT_DIR}/results/${dir}/${SLURM_ARRAY_TASK_ID}
	done
done
# now we can delete the stuff we made here in /scratch/raskli
rm -rf /scratch/${SLURM_JOB_ID}/${SLURM_ARRAY_TASK_ID}/
# i guess it gets deleted automagically?

# now we are done

echo "========= Job finished at `date` =========="
#
