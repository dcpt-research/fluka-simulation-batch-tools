# takes a DICOM free FRES prepared FLUKA folder setup (FLUKA_XXX_PLAN1, FLUKA_XXX_PLAN2 ..... )
# and sets the number of CPUS' per array job
# make sure all input files are created for 1000000 (1m) primaries

# Had a bug recently where the "data" directory was overwritten by text from one of these scripts?

target_dir=$1
number_of_source_files=$(find ${target_dir} -name "source.f" | wc -l)

echo "Copying script files to $target_dir"
cp /home/raskli/scripts/hpc/usbsuw $target_dir
cp /home/raskli/scripts/hpc/usbrea $target_dir
cp /home/raskli/scripts/hpc/sum_file.sh $target_dir
cp /home/raskli/scripts/hpc/txt_file.sh $target_dir
cp /home/raskli/scripts/hpc/fort_to_bnn.sh $target_dir
cp /home/raskli/scripts/hpc/fort_to_bnn_all.sh $target_dir
echo "Copying jobscripts to $target_dir"
cp /home/raskli/scripts/hpc/general_jobscript.sh $target_dir/run_simulation_jobscript.sh
cp /home/raskli/scripts/hpc/general_datamerging_jobscript.sh $target_dir/merge_data_jobscript.sh
cp /home/raskli/scripts/hpc/sim_then_merge_then_wait.sh $target_dir
cp /home/raskli/scripts/hpc/start_merge_then_wait.sh $target_dir
cp /home/raskli/scripts/hpc/run_all.sh $target_dir/data
for fold in $(find $target_dir -name "source.f"); do
	cp /home/raskli/scripts/hpc/run_single_simulation.sh $(dirname $fold)
done

echo "Setting number of CPU's per task to: $number_of_source_files"
sed -i 's/NUMBEROFFIELDS/'"${number_of_source_files}"'/g' $target_dir/run_simulation_jobscript.sh 
sed -i 's/MEMORY/'"$(echo "scale=3;${number_of_source_files} * 2"|bc)G"'/g' $target_dir/run_simulation_jobscript.sh 
sed -i 's/NUMBEROFFIELDS/'"${number_of_source_files}"'/g' $target_dir/merge_data_jobscript.sh 
sed -i 's/EMAIL_ADDRESS/'"$(cat /home/raskli/scripts/email_adress.txt)"'/g' $target_dir/run_simulation_jobscript.sh

echo "Setting number of fields to: $number_of_source_files"
totpath=$(realpath $target_dir)
echo "Changing jobname to: FLUKA_${totpath##*/}"
sed -i 's/JOBNAME/FLUKA_'"${totpath##*/}"'/' $target_dir/run_simulation_jobscript.sh
sed -i 's/JOBNAME/F_mer_'"${totpath##*/}"'/' $target_dir/merge_data_jobscript.sh



