#!/usr/bin/env bash
function usage()
{
    echo "Tool for grabbing all .bnn.lis data from a batch of simulations. "
    echo "The .bnn.lis files have to be in each patient directory under results/BNNLIS"
    echo "We assume patient/simulation names starting with a numeric character."
    echo "Usage:"
    echo "$0 [OPTION] [batches_to_grab_data_from...]"
    echo "OPTIONS:"
    echo "  -h, --help                      Print this help message"
    exit 1
}


if [ $# -eq 0 ]; then
    usage
fi

# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            echo "Unknown option $1"
            usage
            ;;
        *)
            batch_list+=("$1")
            shift
            ;;
    esac
done

not_all_results_done=0
for batch in ${batch_list[@]};
do
    printf "Extracting data for ${batch} .... "
    # First check if results are present for all patients
    for fold in $batch/[0-9]*;
    do
	    if [[ ! -d ${fold}/results/BNNLIS ]];
        then 
            not_all_results_done=1
        fi
    done
    if [[ ${not_all_results_done} -eq 1 ]];
    then
        printf " : Not all data present. Aborting for this batch!\n"
        continue
    fi
    # We know all the data is here for this batch
    mkdir ${batch}/results_sendback -p
    for fold in $batch/[0-9]*;
    do
	    mv ${fold}/results/BNNLIS ${batch}/results_sendback/${fold##*/}_BNNLIS
    done
    bnnlis_dir="BNNLIS_batch_$(echo $batch | cut -d "_" -f2)"
    if [[ ! -d $batch/../${bnnlis_dir} ]];
    then 
        mv $batch/results_sendback $batch/../${bnnlis_dir}
        printf " : OK\n"
        continue
    fi
    printf " : There is already a folder called ${bnnlis_dir}. Aborting for this batch!\n"
done

