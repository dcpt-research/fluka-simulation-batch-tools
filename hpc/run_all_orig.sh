#! /bin/bash

spawns=$1
cycles=$2

for d in ${PWD}/*/
do
     (cd "$d" && test -r run_simulation.sh && ./run_simulation.sh "$spawns" "$cycles")
done
