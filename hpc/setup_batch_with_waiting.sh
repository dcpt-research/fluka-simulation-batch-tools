bat=$1
if [[ $bat == "" ]];
then
	echo "Please supply a path to a batch";
	exit 0
fi
for fold in ${bat}/*;
do
	/home/raskli/scripts/hpc/setup_jobscript_with_waiting.sh ${fold}
done
cp "/home/raskli/scripts/hpc/submit_batch_with_waiting.sh" $bat
