#!/bin/bash

if [ ! -f ${PWD}/$1 ]
then echo " Executable ${FLUPRO}/flutil/$1 don't exist,"
echo " please check the FLUPRO variable in the sum_file.sh script"
exit 1
fi;

function usage()
{
echo "Usage: ./sum_file.sh sumprogram dir_root_name file_extention sum_filename"
echo "       Please enter all parameters"
exit 1
}

if [ -z $1 ]
 then usage; fi
if [ -z $2 ]
 then usage; fi
if [ -z $3 ]
 then usage; fi
if [ -z $4 ]
 then usage; fi

echo "# Build by sum_file.sh" > tot_sum.sh
echo "$PWD/$1  <<EOF " >> tot_sum.sh

for i in `ls *$3`
 do
    echo $i >> tot_sum.sh
 done
echo " " >> tot_sum.sh
echo $4 >> tot_sum.sh
echo "EOF" >> tot_sum.sh
source tot_sum.sh
rm tot_sum.sh
exit
