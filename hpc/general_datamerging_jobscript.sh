#!/bin/bash
#SBATCH --job-name=JOBNAME
#SBATCH --partition=q36,q40,q48
#SBATCH --mem=128G
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=NUMBEROFFIELDS
#SBATCH --time=12:00:00


echo "========= Job started  at `date` =========="

echo "My jobid: $SLURM_JOB_ID"

ml load gcc/9.3.0

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK:-1}
export FX=gfortran-9.3

NFIELDS=NUMBEROFFIELDS


echo "Copying data to /scratch at $(date)"
cd $SLURM_SUBMIT_DIR
mkdir -p /scratch/${SLURM_JOB_ID}
cp -r results /scratch/${SLURM_JOB_ID}
cp sum_file.sh txt_file.sh usbsuw usbrea fort_to_bnn.sh fort_to_bnn_all.sh /scratch/${SLURM_JOB_ID}
cd /scratch/${SLURM_JOB_ID}/
echo "Data has been copied to /scratch at $(date)"

# now we need to put the files in the correct places


# merge all the data

echo "Merging data at $(date)"
i=1
for plan in results/*; 
do 
	for field in $plan/*;
	do
		for fold in $field/*;
		do
			for file in $fold/*; 
			do 
				# this sed looks replaces anything up to the last "_" (including the "_")
				# with whatever was there before + "xyz_" where xyz is the folder number
				# in this case xyz is ${fold##*/} and $fold is something like /home/raskli/..../123
				# and thus ${fold##*/} is just 123.
				# example of this sed: 1b1_lao_1001_fort.40 -> 1b1_lao_1001_123_fort.40
				# just to make all the different data files unique
				mv ${file} ${field}/$(echo ${file##*/} | sed 's,\(.*\)_,\1_'"${fold##*/}"'_,')
			done 
			rm $fold -r 
		done
		cp /scratch/${SLURM_JOB_ID}/usbrea $field
		cp /scratch/${SLURM_JOB_ID}/usbsuw $field
		cp /scratch/${SLURM_JOB_ID}/txt_file.sh $field
		cp /scratch/${SLURM_JOB_ID}/sum_file.sh $field
		cp /scratch/${SLURM_JOB_ID}/fort_to_bnn.sh $field
	done
	cp /scratch/${SLURM_JOB_ID}/fort_to_bnn_all.sh $plan
	cd $plan
	mkdir -p /scratch/${SLURM_JOB_ID}/results/BNNLIS
	./fort_to_bnn_all.sh >> /scratch/${SLURM_JOB_ID}/mergeoutput.txt &
	pids[${i}]=$!
	i=$((i+1))
	cd /scratch/${SLURM_JOB_ID}
done
for pid in ${pids[*]};
do
	wait $pid
done
echo "Data has been merged at $(date)"
echo "Moving the data back to the submit directory"
for file in $(find /scratch/${SLURM_JOB_ID} -name "*.bnn.lis");
do
	mv $file /scratch/${SLURM_JOB_ID}/results/BNNLIS/
done

echo "Data has been merged"
### DATA MERGING DONE

# now copy home the bnn.lis's you've made
mkdir -p ${SLURM_SUBMIT_DIR}/results/BNNLIS
cp /scratch/${SLURM_JOB_ID}/results/BNNLIS/*.bnn.lis ${SLURM_SUBMIT_DIR}/results/BNNLIS/
cp /scratch/${SLURM_JOB_ID}/mergeoutput.txt ${SLURM_SUBMIT_DIR}/results/

# i guess it gets deleted automagically?

# now we are done

echo "========= Job finished at `date` =========="
