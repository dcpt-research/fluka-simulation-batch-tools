#!/bin/bash
#SBATCH --job-name=FLUKA_auto
#SBATCH --partition=q64,q48,q40
#SBATCH --mem=64G
#SBATCH --cpus-per-task=2
# Walltime should be high enough that this makes sense
#SBATCH --time=120:00:00
#SBATCH --export=NONE



echo "========= Job started  at `date` =========="

echo "My jobid: $SLURM_JOB_ID"

echo "time:$(date) --- job: $SLURM_JOB_ID --> user : $(whoami) --- master_jobscript"
cd ${SLURM_SUBMIT_DIR}

batches_ready_to_go=${SLURM_SUBMIT_DIR}/../ready_for_simulation.txt
ready_job_locations=${SLURM_SUBMIT_DIR}/../ready_runs/
if [[ $(head -1 $batches_ready_to_go) == "" ]];
then
	# There are no available jobs. Stopping.
	echo "No available jobs in $batches_ready_to_go"
	echo "Exiting"
	exit
fi
jobname=$(head -1 $batches_ready_to_go)

echo "Copy the batch into active simulation under scratch"
mkdir -p /scratch/${SLURM_JOB_ID}/active_simulation
path_to_ready_batch=$(head -1 $batches_ready_to_go)
if [[ $(echo $path_to_ready_batch | grep ".tar.gz") == "" ]]; # if it doesn't end in .tar.gz we add it
then
	path_to_ready_batch=${path_to_ready_batch}.tar.gz
else # it is ending in .tar.gz, and thus jobname should have a different name (without tar.gz)
	jobname=$(echo ${jobname} | sed -e 's/\.tar\.gz//')
fi
cp -r ${ready_job_locations}/${path_to_ready_batch} /scratch/${SLURM_JOB_ID}/active_simulation/
job_location=/scratch/${SLURM_JOB_ID}/active_simulation/${jobname}

echo "Unzipping the archive"
cd /scratch/${SLURM_JOB_ID}/active_simulation/
mv ${jobname}.tar.gz ${jobname}
#tar xvzf ${jobname}.tar.gz > unpacking_${SLURM_JOB_ID}.out
#rm ${jobname}.tar.gz

echo "Copying necessary scripts to scratch"
mkdir -p /scratch/${SLURM_JOB_ID}/scripts
cp /home/raskli/scripts/hpc/grab_bnn_data.sh /scratch/${SLURM_JOB_ID}/scripts


# Countd -7 $pat/run_simulation_jobscript.sh | tail -1 | cut -d "=" -f2)a$(head -7 $pat/run_simulation_jobscript.sh | tail -1 | cut -d "=" -f2)jkk up the number of cores per task / iteration and choose a fitting max cores per job at a time
total_cores_sum=0;
for pat in $job_location/[0-9]*; 
do 
	this_patient_cores=$(head -7 $pat/run_simulation_jobscript.sh | tail -1 | cut -d "=" -f2)
	total_cores_sum=$((total_cores_sum+this_patient_cores)) 
done
cores_per_patient=$(echo "300/$total_cores_sum" | bc)
echo "Running max $cores_per_patient runs per simulation"
for pat in $job_location/[0-9]*; 
do 
	sed -i 's/%.*/%'${cores_per_patient}'/' $pat/run_simulation_jobscript.sh
	sed -i 's/%.*/%8/' $pat/run_simulation_jobscript.sh
done


echo "Submitting the job at $(date)"
cd $job_location
# Here we should call submit_batch_then_wait.sh
echo "Submitting job "submit_batch_then_wait.sh" ... "
./submit_batch_then_wait.sh
wait
echo ""submit_batch_then_wait.sh" done!"
echo "All simulation is completed at $(date)"
echo "Checking whoami = $(whoami)"

# Zip up the completed job and put in completed
echo "Grabbing data from $jobname at $job_location"
echo "Looking into job_location"
/scratch/${SLURM_JOB_ID}/scripts/grab_bnn_data.sh $job_location
mv $job_location/*.out $job_location/*.txt results_sendback
mv results_sendback results_sendback_$jobname
mkdir -p /scratch/$SLURM_JOB_ID/for_compression

echo "Zip up the data in results_sendback"
cp -r results_sendback_$jobname /scratch/$SLURM_JOB_ID/for_compression
cd /scratch/$SLURM_JOB_ID/for_compression
tar -czvf results_sendback_${jobname}.tar.gz results_sendback_$jobname
mv results_sendback_${jobname}.tar.gz ${SLURM_SUBMIT_DIR}

cd ${SLURM_SUBMIT_DIR}
rm -r /scratch/$SLURM_JOB_ID/for_compression
echo "Move the zipped data from active simulation directory to completed"
mv results_sendback_${jobname}.tar.gz ${SLURM_SUBMIT_DIR}/../completed_runs

echo "Remove everything but the zipped results"
rm -rf /scratch/${SLURM_JOB_ID}/scripts
rm -rf /scratch/${SLURM_JOB_ID}/active_simulation
rm -rf /scratch/$SLURM_JOB_ID/for_compression

echo "Remove this simulation from the file: $batches_ready_to_go"
tail -n +2 $batches_ready_to_go > temp_jobs.tmp
mv temp_jobs.tmp $batches_ready_to_go
rm temp_jobs.tmp


echo "Now this simulation is done"

# Check if there are other available jobs
if [[ $(cat $batches_ready_to_go) != "" ]];
then
	echo "We can use the batch: $(head -1 $batches_ready_to_go)"
	echo "Start this jobscript again, which will now run with the next simulation data"
	sbatch master_jobscript.sh
fi

echo "========= Job finished at `date` =========="
