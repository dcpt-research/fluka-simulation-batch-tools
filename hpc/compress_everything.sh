if [[ $1 != "--goahead" ]];
then
	echo "You should know exactly what this does or not use it at all"
	echo "Exiting."
	exit 0
fi
# You should fill in the correct filenames for this
for fold in BNNLIS_batch_[0-9]*; do mkdir ${fold}_wrapper; mv $fold ${fold}_wrapper; cp /home/raskli/scripts/hpc/compression_jobscript.sh ${fold}_wrapper; sed -i 's/FOLDNAME/'${fold}'/g' ${fold}_wrapper/compression_jobscript.sh; done

base=$(pwd)
for fold in *_wrapper; do cd ${fold}; sbatch compression_jobscript.sh; cd $base; done

