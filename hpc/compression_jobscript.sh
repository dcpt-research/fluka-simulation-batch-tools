#!/bin/bash
#SBATCH --job-name=data_compression
#SBATCH --partition=q48,q28
#SBATCH --mem=128G
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=2
#SBATCH --time=8:00:00


echo "========= Job started  at `date` =========="

echo "My jobid: $SLURM_JOB_ID"


echo "Copying data to /scratch at $(date)"
cd $SLURM_SUBMIT_DIR
mkdir -p /scratch/${SLURM_JOB_ID}
cp -r FOLDNAME /scratch/${SLURM_JOB_ID}
cd /scratch/${SLURM_JOB_ID}/
echo "Data has been copied to /scratch at $(date)"
tar -czf FOLDNAME.tar.gz FOLDNAME

echo "Data has been compressed"

# now copy home the compressed files you've made
cp /scratch/${SLURM_JOB_ID}/FOLDNAME.tar.gz ${SLURM_SUBMIT_DIR}/FOLDNAME.tar.gz


echo "========= Job finished at `date` =========="
